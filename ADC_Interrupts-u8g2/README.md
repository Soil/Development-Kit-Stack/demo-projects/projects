# ADC_Interrupts-u8g2

In this, the X, Y coordinates are displayed on OLED screen by the use of ADC interrupts.

## Libraries Used

* U8G2lib.h - For OLED screen

## External Pins Used

* Joystick X Axis - P1_0
* Joystick Y Axis - P1_1