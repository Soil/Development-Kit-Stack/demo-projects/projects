/*

  PrintUTF8.ino
  
  Use the (Arduino compatible) u8g2 function "print"  to draw a text.

  Universal 8bit Graphics Library (https://github.com/olikraus/u8g2/)

  Copyright (c) 2016, olikraus@gmail.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification, 
  are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice, this list 
    of conditions and the following disclaimer.
    
  * Redistributions in binary form must reproduce the above copyright notice, this 
    list of conditions and the following disclaimer in the documentation and/or other 
    materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  

*/

/* --COPYRIGHT--,BSD_EX
   Copyright (c) 2012, Texas Instruments Incorporated
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

 * *  Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

 * *  Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

 * *  Neither the name of Texas Instruments Incorporated nor the names of
      its contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
   OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *******************************************************************************

                         MSP430 CODE EXAMPLE DISCLAIMER

   MSP430 code examples are self-contained low-level programs that typically
   demonstrate a single peripheral function or device feature in a highly
   concise manner. For this the code may rely on the device's power-on default
   register values and settings such as the clock configuration and care must
   be taken when combining code from several examples to avoid potential side
   effects. Also see www.ti.com/grace for a GUI- and www.ti.com/msp430ware
   for an API functional library-approach to peripheral configuration.

   --/COPYRIGHT--*/

//******************************************************************************
//  MSP430FR59xx Demo - ADC12, Window Comparator, 2.5V ref
//
//  Description; A1 is sampled in single ch/ single conversion mode.
//  Window comparator is used to generate interrupts to
//  indicate when the input voltage goes above the High_Threshold or below the
//  Low_Threshold or is in between the high and low thresholds. TimerB0 is used
//  as an interval timer used to control the LED at P1.0 to toggle slow/fast
//  or turn off according to the ADC12 Hi/Lo/IN interupts.
//
//               MSP430FR5969
//            -----------------
//        /|\|              XIN|-
//         | |                 | 32kHz
//         --|RST          XOUT|-
//           |                 |
//       >---|P1.1/A1     	   |
// 		 >---|P1.1/A1 		   |
//
//   Priya Thanigai
//   Texas Instruments Inc.
//   February 2014
//   Built with IAR Embedded Workbench V5.60 & Code Composer Studio V5.5
//******************************************************************************


#include "msp430.h"
#include <Arduino.h>
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

// U8g2 Contructor List (Frame Buffer)
// The complete list is available here: https://github.com/olikraus/u8g2/wiki/u8g2setupcpp
// Please update the pin numbers according to your setup. Use U8X8_PIN_NONE if the reset pin is not connected

U8G2_SH1106_128X64_NONAME_F_4W_HW_SPI u8g2(U8G2_R0, /* cs=*/ P3_5, /* dc=*/ P3_4, /* reset=*/ U8X8_PIN_NONE);

//.......................Thresholds for the interrupts
#define High_Threshold 0xCCC         // ~2V
#define Low_Threshold  0x333         // ~1V

//.......................X_pin connected to A0,Y_pin connected to A1
int ADC12IVtemp, X_pin, Y_pin;
//.......................Change these values to change the sensitivity of X, Y individually
const int X_High = 3272, X_Low = 1900, Y_High = 818, Y_Low = 375;

void setup()
{
analogReadResolution(10);
  //.....................Configure ADC P1.1/A1
  P1SEL1 |= BIT1;
  P1SEL0 |= BIT1;
  //.....................Configure ADC P1.0/A0
  P1SEL1 |= BIT0;
  P1SEL0 |= BIT0;
  //..........................................

  //.....................Configure ADC12

  // software trigger for SOC, MODOSC, single ch-single conversion,
  // tsample controlled by SHT0x settings
  // Channel 1, reference = internal, enable window comparator
  // Set thresholds for ADC12 interrupts
  // Enable Interrupts
  ADC12CTL0 = ADC12SHT0_2 | ADC12MSC | ADC12ON;          // tsample = 16ADC12CLK cycles, tconvert = 14 ADC12CLK cycles
  ADC12CTL1 = ADC12CONSEQ_1 | ADC12SHP;
  ADC12CTL2 |= ADC12RES_2;                               // 12-bit conversion
  ADC12MCTL0 = ADC12INCH_1 | ADC12VRSEL_1 | ADC12WINC;
  ADC12HI = High_Threshold;
  ADC12LO = Low_Threshold;
  ADC12IER2 = ADC12HIIE | ADC12LOIE | ADC12INIE;

  // Configure internal reference
  while (REFCTL0 & REFGENBUSY);             // If ref generator busy, WAIT
  REFCTL0 |= REFVSEL_2 | REFON;             // Select internal ref = 2.5V
  // Internal Reference ON
  while (!(REFCTL0 & REFGENRDY));           // Wait for reference generator to settle

  u8g2.begin();
  u8g2.enableUTF8Print();    // enable UTF8 support for the Arduino print() function

}

void loop()
{
  u8g2.setFont(u8g2_font_unifont_t_chinese2);  // use chinese2 for all the glyphs of "你好世界"
  u8g2.setFontDirection(0);
  ADC12CTL0 |= ADC12ENC | ADC12SC;        // Enable & start conversion
}
void temp()
{

  //if (ADC12IVtemp == 6)
  {
    u8g2.clearBuffer();
    if (X_pin > X_High)
    {
      u8g2.setCursor(0, 15);
      u8g2.print("HIGH_X");
    }
    else {
      if (X_pin < X_High && X_pin > X_Low)
      {
        u8g2.setCursor(0, 15);
        u8g2.print("MID_X");
      }
      else
      {
        u8g2.setCursor(0, 15);
        u8g2.print("LOW_X");
      }
    }
    if (Y_pin > Y_High)
    {
      u8g2.setCursor(40, 40);
      u8g2.print("HIGH_Y");
    }
    else
    {
      if (Y_pin < Y_High && Y_pin > Y_Low)
      {
        u8g2.setCursor(40, 40);
        u8g2.print("MID_Y");
      }
      else
      {
        u8g2.setCursor(40, 40);
        u8g2.print("LOW_Y");
      }
    }
    u8g2.sendBuffer();
  }
}
#pragma vector = ADC12_VECTOR
__interrupt void ADC12_ISR(void)
{
  ADC12IVtemp = ADC12IV;
  X_pin = ADC12MEM0;// Save MEM0
  Y_pin = ADC12MEM1;// Save MEM1
  temp();
}
