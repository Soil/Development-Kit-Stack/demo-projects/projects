# Space Trash Tutorial

This is a space invader game. In this tutorial, user can edits the specific aspects of the game according to his needs. Use the joystick to move the spaceship, and fire.

## Libraries Used

* U8G2lib.h - For OLED screen
* spaceTrooper.h - For space invader game
* space-bitmaps.h - Contains the bitmaps of various objects used in the game

## External Pins Used

* Joystick push button - P2_2
* Joystick X Axis - P1_3
* Joystick Y Axis - P1_1