#include "spaceTrooper.h"

#define st_bitmap_player1_width 7				/**< Spaceship bitmaps's width*/
#define st_bitmap_player1_height 5				/**< Spaceship bitmap's height*/

const uint8_t st_bitmap_player1[]  =
{
  /* 01100000 */ 0x060,
  /* 11111000 */ 0x0f8,
  /* 01111110 */ 0x07e,
  /* 11111000 */ 0x0f8,
  /* 01100000 */ 0x060
};												/**< bitmap of spaceship. Edit This bitmap to your object for spaceship. Also edit the width and height of the object.*/

/**
 * @brief      Draws the objects in the game.
 * @param  objnr  The object number
 * @returns null
 */
void st_DrawObj(uint8_t objnr)
{
  st_obj *o = st_GetObj(objnr);
  switch (u8x8_pgm_read(&(st_object_types[o->ot].draw_fn)))
  {
    case ST_DRAW_NONE:
      break;
    case ST_DRAW_BBOX:
      st_DrawBBOX(objnr);
      break;
    case ST_DRAW_TRASH1:
      st_DrawBitmap(objnr, st_bitmap_trash_5x5_1, o->x1 - o->x0 + 1, 5);
      break;
    case ST_DRAW_TRASH2:
      st_DrawBitmap(objnr, st_bitmap_trash_5x5_2, o->x1 - o->x0 + 1, 5);
      break;
    case ST_DRAW_BIG_TRASH:
      st_DrawBitmap(objnr, st_bitmap_trash_7x7, o->x1 - o->x0 + 1, 7);
      break;
    case ST_DRAW_PLAYER1:
      st_DrawBitmap(objnr, st_bitmap_player1, st_bitmap_player1_width, st_bitmap_player1_height);
      break;
    case ST_DRAW_PLAYER2:
      st_DrawBitmap(objnr, st_bitmap_player2, 7, 8);
      break;
    case ST_DRAW_PLAYER3:
      st_DrawBitmap(objnr, st_bitmap_player3, 7, 11);
      break;
    case ST_DRAW_GADGET:
      /* could use this proc, but... */
      /* st_DrawBitmap(objnr, st_bitmap_gadget,o->x1-o->x0+1, 5); */
      /* ... this one looks also funny. */
      st_DrawBitmap(objnr, st_bitmap_gadget, 5, 5);
      break;
    case ST_DRAW_BACKSLASH:
      {
        uint8_t x;
        uint8_t y;
        x = st_CalcXY(o);
        y = st_px_y;


        // dog_SetPixel(x,y);
        // x++; y--;
        // dog_SetPixel(x,y);
        // x++; y--;
        // dog_SetPixel(x,y);

        u8g2_SetDrawColor(st_u8g2, 1);
        u8g2_DrawPixel(st_u8g2, x, u8g_height_minus_one - y);
        x++; y--;
        u8g2_DrawPixel(st_u8g2, x, u8g_height_minus_one - y);
        x++; y--;
        u8g2_DrawPixel(st_u8g2, x, u8g_height_minus_one - y);
      }
      break;
    case ST_DRAW_SLASH:
      {
        uint8_t x;
        uint8_t y;
        x = st_CalcXY(o);
        y = st_px_y;

        // dog_SetPixel(x,y);
        // x++; y++;
        // dog_SetPixel(x,y);
        // x++; y++;
        // dog_SetPixel(x,y);

        u8g2_SetDrawColor(st_u8g2, 1);
        u8g2_DrawPixel(st_u8g2, x, u8g_height_minus_one - y);
        x++; y++;
        u8g2_DrawPixel(st_u8g2, x, u8g_height_minus_one - y);
        x++; y++;
        u8g2_DrawPixel(st_u8g2, x, u8g_height_minus_one - y);
      }
      break;
  }
}

/**
 * @brief      Function to check if spaceship wants to fire
 * @param      null
 * @return     1 = Fire!	0 = Don't Fire
 */
int fire()
{
  if (analogRead(Y_pin) > Y_High)
    return 1;
  else
    return 0;
}

/**
 * @brief      Function to check if spaceship wants to go UP
 * @param      null
 * @return     1 = Go UP!	0 = Don't go Up
 */
bool isUp()
{
  if (analogRead(X_pin) < X_Low)
  {
    return true;
  }
  else
    return false;
}

/**
 * @brief      Function to check if spaceship wants to go Left
 * @param      null
 * @return     1 = Go Left!	0 = Don't go Left
 */
bool isLeft()
{
  if (analogRead(Y_pin) < Y_Low)
  {
    return true;
  }
  else
    return false;
}

/**
 * @brief      Function to check if spaceship wants to go Right
 * @param      null
 * @return     1 = Go Right!	0 = Don't go Right
 */
bool isRight()
{
  if (analogRead(Y_pin) > Y_High)
  {
    return true;
  }
  else
    return false;
}

/**
 * @brief      Function to check if spaceship wants to go Down
 * @param      null
 * @return     1 = Go Down!	0 = Don't go Down
 */
bool isDown()
{
  if (analogRead(X_pin) > X_High)
  {
    return true;
  }
  else
    return false;
}

/**
 * @brief      Function to check if joystick button is pressed
 * @param      null
 * @return     1 = Pressed 		0 = Not pressed
 */
bool isClick()
{
  if (digitalRead(SW_pin) == 0)
  {
    return true;
  }
  else
    return false;
}

/**
 * @brief      Moves the spaceship Up or Down depending on the joystick position
 * @param      null
 * @returns    null
 */
void shipMovement()
{
  if (isUp())
  {
    y++;  //Move space ship UP
  }

  if (isDown())
  {
    y--;  //Move space ship DOWN
  }
}

void setup()
{
  spaceInit();								// Initialize Space Invader game
}

void loop()
{
  st_Setup(u8g2.getU8g2());
  for (;;)
  {
    st_Step(y, /* is_auto_fire */ 0, /* is_fire */ fire());
    u8g2.firstPage();
    do
    {
      st_Draw(0);
    } while ( u8g2.nextPage() );

    shipMovement();

    if (st_player_points >= k * i)
    {
      k += 2;								             //constant for stage clearing difficuly balancing.
      u8g2.clearBuffer();
      u8g2.setFont(u8g_font_6x10); 			//Font list available in u8g2.h
      u8g2.setCursor(0, 15);
      u8g2.print("Stage Cleared!");
      u8g2.setCursor(0, 40);
      u8g2.print(i);
      u8g2.sendBuffer();
      i++;
      delay(1000);
    }
  }
}
