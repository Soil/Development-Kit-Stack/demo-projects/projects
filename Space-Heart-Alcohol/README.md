# Space-Heart-Alcohol

This is a demo project to show the use of OLED sceen, pulse sensor and alcohol sensor.

- Space Invader: use joystick to play the game. At the end of stage two/ game over/ click of button, the pulse sensor code will run.

- Pulse Sensor: Keep your first finger on the sensor. Usually the BMP will vary between 60 to 120. Depending on the BPM, emoji will appear. Click the joystick to go to alchohol sensor.

- Alcohol Sensor: The instructions on the screen will guide you through the tutorial.

## Libraries Used

* U8G2lib.h - For OLED screen
* hcrs04.h - For ultrasonic Sensor

## External Pins Used

* Joystick push button - P2_2
* Joystick X Axis - P1_3
* Joystick Y Axis - P1_1
* Ultrasonic Trigger pin - P4_6
* Ultrasonic Echo pin - P4_5
* MQ135 pin - P1_4
* Moisture sensor - P1_2
* Pulse sensor - P1_0