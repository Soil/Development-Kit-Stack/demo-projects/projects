/*

  SpaceTrash.ino

  Universal 8bit Graphics Library (https://github.com/olikraus/u8g2/)

  Copyright (c) 2016, olikraus@gmail.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list
    of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright notice, this
    list of conditions and the following disclaimer in the documentation and/or other
    materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "hcrs04.h"
#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

// U8g2 Contructor List (Picture Loop Page Buffer)
// The complete list is available here: https://github.com/olikraus/u8g2/wiki/u8g2setupcpp
// Please update the pin numbers according to your setup. Use U8X8_PIN_NONE if the reset pin is not connected

U8G2_SH1106_128X64_NONAME_F_4W_HW_SPI u8g2(U8G2_R0, /* cs=*/ P3_5, /* dc=*/ P3_4, /* reset=*/ U8X8_PIN_NONE);

#define VCCSTATE SH1106_SWITCHCAPVCC
#define WIDTH     128
#define HEIGHT     64
#define NUM_PAGE    8  /* number of pages */

/* ultrasonic sensor */
const int PINTRIG = P4_6;
const int PINECHO = P4_5;
hcrs04 ultrasonic(PINTRIG, PINECHO);

/*      MQ-135       */
const int MQsensorPin = P1_4;          //
int MQsensorData = 0;
int MQsensorValue = 0;

/*   moisture sensor     */
const int MoistureSensorPin = P1_2;   //
int MoistureSensordata = 0;
int MoistureSensorValue = 0;


uint8_t oled_buf[WIDTH * HEIGHT / 8];
boolean introdone = false; // intro sequence flag
int newintro = 1; //intro sequence mapping
boolean yes = false;
boolean no = false;
boolean back = false;
int b_last;//last intro to be played
int t = 7;
bool negative = false;
const int ctsPin = P1_0;
const int espSS = P4_0;



int DISTANCE;
int u = 0;
int DISTANCEupper;
int DISTANCElower;

char D[5];
// Function to start intro sequence
const int ACKpin = P1_5;                  // Acknowledge pin
int ACKState = 0;                  // Acknowledge status


static const unsigned char gasUI [] PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3c,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3c, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xbd, 0x03, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0xc0, 0xff, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0xc0, 0xe3, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xc0, 0xc3, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe7,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xff, 0x01, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x80, 0xff, 0x01, 0x00, 0x00, 0x00, 0x00, 0xc0,
  0x03, 0x00, 0x1e, 0xe7, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfc, 0x3f, 0x00,
  0x3e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0x80, 0xbe, 0x01,
  0x00, 0x00, 0x00, 0x00, 0x80, 0xff, 0xff, 0xc1, 0xff, 0x01, 0x00, 0x00,
  0x00, 0x00, 0xc0, 0xff, 0xff, 0xe3, 0xe3, 0xc3, 0x03, 0x00, 0x00, 0x00,
  0xe0, 0xff, 0xff, 0xe7, 0xe3, 0xc3, 0x07, 0x00, 0x00, 0x00, 0xe0, 0xff,
  0xff, 0xc7, 0xe3, 0xd1, 0x37, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x07,
  0x77, 0xf8, 0x3f, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x87, 0xff, 0x7c,
  0x7e, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0xc7, 0xff, 0x7d, 0x7c, 0x00,
  0x00, 0x00, 0xc0, 0xff, 0xff, 0x87, 0xf7, 0x7c, 0x3c, 0x00, 0x00, 0x00,
  0x80, 0xff, 0xff, 0x03, 0x02, 0xe0, 0x2f, 0x85, 0x01, 0x00, 0x00, 0xff,
  0xff, 0x00, 0x00, 0xf0, 0x3f, 0xc1, 0x3f, 0x00, 0x00, 0xfe, 0x7f, 0x00,
  0x00, 0xf8, 0x3f, 0xf0, 0x3f, 0x00, 0x00, 0xf0, 0x0f, 0x00, 0x00, 0xf0,
  0x1e, 0xf8, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0x0c, 0xf0,
  0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf0, 0xf0, 0x00,
  0x80, 0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0xff, 0xf9, 0x01, 0xf0, 0xff,
  0xff, 0xff, 0x03, 0x00, 0xc0, 0xff, 0xff, 0x01, 0xf8, 0xff, 0xff, 0xff,
  0x07, 0x00, 0xe0, 0xff, 0xff, 0x03, 0xfc, 0xff, 0xff, 0xff, 0x0f, 0x00,
  0xf8, 0xff, 0xff, 0x00, 0xfe, 0xff, 0xff, 0xff, 0x1f, 0x00, 0xfc, 0xff,
  0x0f, 0x00, 0xfe, 0xff, 0xff, 0xff, 0x7f, 0x00, 0xfe, 0x3f, 0x00, 0x00,
  0xfe, 0xff, 0xff, 0xff, 0xff, 0x80, 0xff, 0x1f, 0x00, 0x00, 0xfe, 0xff,
  0xff, 0xff, 0xff, 0xc3, 0xff, 0x07, 0x00, 0x00, 0xfe, 0xff, 0xff, 0xff,
  0xff, 0xe7, 0xff, 0x03, 0x00, 0x00, 0xfe, 0xff, 0xff, 0xff, 0xff, 0xff,
  0xff, 0x01, 0x00, 0x00, 0xfe, 0xff, 0xff, 0x3f, 0xfe, 0xff, 0x7f, 0x00,
  0x00, 0x00, 0xfe, 0xff, 0xff, 0x3f, 0xfc, 0xff, 0x3f, 0x00, 0x00, 0x00,
  0xfe, 0xff, 0xff, 0x7f, 0xf8, 0xff, 0x0f, 0x00, 0x00, 0x00, 0xfe, 0xff,
  0xff, 0x7f, 0xe0, 0xff, 0x07, 0x00, 0x00, 0x00, 0xfe, 0xff, 0xff, 0x7f,
  0xc0, 0xff, 0x03, 0x00, 0x00, 0x00, 0xfe, 0xff, 0xff, 0x7f, 0x80, 0xff,
  0x00, 0x00, 0x00, 0x00, 0xfe, 0xff, 0xff, 0x7f, 0x00, 0x3e, 0x00, 0x00,
  0x00, 0x00, 0xfe, 0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xfe, 0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfe, 0xff,
  0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfe, 0xff, 0xff, 0x7f,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfe, 0xff, 0xff, 0x7f, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0xfe, 0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0xfe, 0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xfe, 0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfe, 0xff,
  0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfe, 0xff, 0xff, 0x7f,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfe, 0xff, 0xff, 0x7f, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0xfc, 0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0xf8, 0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xf0, 0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff,
  0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0x7f,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0x3f, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00
};

static const unsigned char UI_gas_smelly [] PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x40, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07,
  0x00, 0x20, 0x1c, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf0, 0x3f, 0x00, 0x20,
  0x8c, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfc, 0xff, 0x00, 0x10, 0x8e, 0x00,
  0x00, 0x00, 0x00, 0x02, 0xfe, 0xff, 0x01, 0x18, 0x87, 0x00, 0x00, 0x00,
  0x10, 0x03, 0xff, 0xff, 0x03, 0x08, 0x87, 0x00, 0x00, 0x00, 0x10, 0x01,
  0xff, 0xff, 0x03, 0x8c, 0xc3, 0x00, 0x00, 0x00, 0x88, 0x01, 0xff, 0xff,
  0x03, 0x84, 0x63, 0x00, 0x00, 0x00, 0xc4, 0x00, 0xff, 0xff, 0x03, 0xc4,
  0x61, 0x00, 0x00, 0x00, 0xc4, 0x08, 0xfe, 0xff, 0x03, 0xc4, 0x31, 0x00,
  0x00, 0x00, 0x62, 0x00, 0xfc, 0xff, 0x01, 0xc4, 0x30, 0x00, 0x02, 0x00,
  0x62, 0x04, 0xf8, 0x7f, 0x00, 0xc4, 0x18, 0x00, 0x3f, 0x00, 0x20, 0x02,
  0xc0, 0x1f, 0x00, 0xc0, 0x18, 0x80, 0x7f, 0x00, 0x20, 0x02, 0x00, 0x00,
  0x00, 0xc0, 0x08, 0x80, 0x63, 0x00, 0x20, 0x02, 0x00, 0x00, 0x00, 0xc0,
  0x00, 0x80, 0xe1, 0x00, 0x20, 0x00, 0xfc, 0xff, 0x1f, 0x80, 0x10, 0xf0,
  0xe3, 0x01, 0x00, 0x00, 0xff, 0xff, 0xff, 0x00, 0x00, 0xfc, 0xff, 0x01,
  0x00, 0x80, 0xff, 0xff, 0xff, 0x01, 0x00, 0xfe, 0xff, 0x01, 0x00, 0xc0,
  0xff, 0xff, 0xff, 0x03, 0x80, 0xff, 0x1f, 0x00, 0x00, 0xe0, 0xff, 0xff,
  0xff, 0x07, 0xc0, 0xff, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0xff, 0x1f,
  0xe0, 0x7f, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0xff, 0x7f, 0xf8, 0x1f,
  0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x0f, 0x00, 0x00,
  0x00, 0xe0, 0xff, 0xff, 0x1f, 0xff, 0xff, 0x03, 0x00, 0x00, 0x00, 0xe0,
  0xff, 0xff, 0x1f, 0xfe, 0xff, 0x01, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff,
  0x1f, 0xfc, 0x7f, 0x00, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x3f, 0xf8,
  0x3f, 0x00, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x3f, 0xf0, 0x1f, 0x00,
  0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x3f, 0xc0, 0x07, 0x00, 0x00, 0x00,
  0x00, 0xe0, 0xff, 0xff, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0,
  0xff, 0xff, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff,
  0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x3f, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x3f, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0xe0, 0xff, 0xff, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0,
  0xff, 0xff, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff,
  0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0xff, 0xff, 0x3f, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0xff, 0xff, 0x3f, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x80, 0xff, 0xff, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0xf8, 0xff, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xf8, 0xff, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf8, 0xff,
  0x1f, 0x00, 0x00, 0x00, 0x00, 0x00
};



static const unsigned char rock_emoji_bits[] PROGMEM = {
  0x00, 0x00, 0x00, 0xff, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf0, 0xff,
  0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfe, 0x01, 0x7f, 0x00, 0x00, 0x00,
  0x00, 0x80, 0x0f, 0x00, 0xf0, 0x01, 0x00, 0x00, 0x00, 0xe0, 0x03, 0x00,
  0xc0, 0x07, 0x00, 0x00, 0x00, 0xf0, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00,
  0x00, 0x38, 0x00, 0x00, 0x00, 0x3c, 0x00, 0x00, 0x00, 0x1e, 0x00, 0x00,
  0x00, 0x78, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0xe0, 0x00, 0x00,
  0x80, 0x03, 0x00, 0x00, 0x00, 0xc0, 0x01, 0x00, 0x80, 0x03, 0x00, 0x00,
  0x00, 0x80, 0x03, 0x00, 0xc0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00,
  0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x60, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x0e, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x00,
  0x38, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x18, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x18, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x00,
  0x0c, 0x00, 0x01, 0x00, 0x00, 0x01, 0x30, 0x00, 0x0c, 0x80, 0x07, 0x00,
  0xc0, 0x03, 0x30, 0x00, 0x0c, 0x00, 0x1e, 0x00, 0xf0, 0x00, 0x60, 0x00,
  0x06, 0x00, 0x38, 0x00, 0x38, 0x00, 0x60, 0x00, 0x06, 0x00, 0x70, 0x00,
  0x0c, 0x00, 0x60, 0x00, 0x06, 0x00, 0x60, 0x00, 0x04, 0x00, 0x60, 0x00,
  0x06, 0x00, 0x40, 0x00, 0x02, 0x00, 0xe0, 0x00, 0x06, 0x00, 0x88, 0x00,
  0x22, 0x00, 0xe0, 0x00, 0x06, 0x00, 0x3e, 0x00, 0x78, 0x00, 0xc0, 0x00,
  0x07, 0x00, 0x3e, 0x00, 0xf8, 0x00, 0xc0, 0x00, 0x07, 0x00, 0x7f, 0x00,
  0xfc, 0x00, 0xc0, 0x00, 0x06, 0x00, 0x7f, 0x00, 0xfc, 0x01, 0xc0, 0x00,
  0x06, 0x00, 0x7f, 0x00, 0xfc, 0x01, 0xe0, 0x00, 0x06, 0x00, 0x7f, 0x00,
  0xfc, 0x01, 0x60, 0x00, 0x06, 0x00, 0x3f, 0x00, 0xfc, 0x00, 0x60, 0x00,
  0x06, 0x00, 0x3e, 0x00, 0xf8, 0x00, 0x60, 0x00, 0x0e, 0x00, 0x1e, 0x00,
  0x78, 0x00, 0x60, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0x00,
  0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x1c, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x30, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x00,
  0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x00, 0x30, 0x00, 0x00, 0xfe,
  0x00, 0x00, 0x1c, 0x00, 0x70, 0x00, 0x80, 0xff, 0x01, 0x00, 0x0c, 0x00,
  0x60, 0x00, 0xc0, 0xff, 0x03, 0x00, 0x0e, 0x00, 0xe0, 0x00, 0xc0, 0xff,
  0x07, 0x00, 0x07, 0x00, 0xc0, 0x01, 0xe0, 0x81, 0x07, 0x80, 0x03, 0x00,
  0x80, 0x03, 0x20, 0x00, 0x04, 0x80, 0x03, 0x00, 0x00, 0x07, 0x00, 0x00,
  0x00, 0xc0, 0x01, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x00, 0xe0, 0x00, 0x00,
  0x00, 0x1c, 0x00, 0x00, 0x00, 0x78, 0x00, 0x00, 0x00, 0x38, 0x00, 0x00,
  0x00, 0x3c, 0x00, 0x00, 0x00, 0xf0, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00,
  0x00, 0xc0, 0x03, 0x00, 0xc0, 0x07, 0x00, 0x00, 0x00, 0x80, 0x1f, 0x00,
  0xf8, 0x01, 0x00, 0x00, 0x00, 0x00, 0xfe, 0xc3, 0x7f, 0x00, 0x00, 0x00,
  0x00, 0x00, 0xf0, 0xff, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7e,
  0x00, 0x00, 0x00, 0x00
};


static const unsigned char calm_icon_bits[] PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3e,
  0x80, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2f, 0xf0, 0x03, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x21, 0x10, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x21,
  0x10, 0x02, 0x00, 0xff, 0x3f, 0x00, 0x00, 0x21, 0x10, 0x02, 0xe0, 0xff,
  0xff, 0x01, 0x00, 0x21, 0x10, 0x02, 0xf8, 0x01, 0xc0, 0x0f, 0x00, 0x39,
  0x90, 0x03, 0x3e, 0x00, 0x00, 0x1e, 0xc0, 0x39, 0x90, 0x03, 0x0f, 0x00,
  0x00, 0x78, 0xe0, 0x01, 0x9e, 0x83, 0x03, 0x00, 0x00, 0xe0, 0xc0, 0x01,
  0x1e, 0xe0, 0x01, 0x00, 0x00, 0xc0, 0x01, 0x00, 0x00, 0x70, 0x00, 0x00,
  0x00, 0x80, 0x03, 0x00, 0x00, 0x38, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00,
  0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x0c, 0x00, 0x00,
  0x00, 0x00, 0x1c, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x38, 0x00,
  0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x03, 0x00, 0x00,
  0x00, 0x00, 0x70, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x60, 0x00,
  0x80, 0x01, 0x30, 0x00, 0x00, 0x06, 0xc0, 0x00, 0x80, 0x01, 0x0c, 0x00,
  0x00, 0x18, 0xc0, 0x00, 0xc0, 0x00, 0x03, 0x00, 0x00, 0x60, 0xc0, 0x01,
  0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x01, 0xc0, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x80, 0x01, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x01,
  0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x03, 0x60, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x03, 0x60, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x03,
  0x60, 0x00, 0x01, 0x03, 0x60, 0x60, 0x00, 0x03, 0x60, 0x00, 0xff, 0x01,
  0xc0, 0x7f, 0x00, 0x03, 0x60, 0x00, 0xfe, 0x00, 0x80, 0x3f, 0x00, 0x03,
  0x60, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x03, 0x60, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x03, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
  0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x01, 0xe0, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x80, 0x01, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x01,
  0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x01, 0xc0, 0x00, 0x00, 0x00,
  0x00, 0x00, 0xc0, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0xc0, 0x00,
  0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0xe0, 0x00, 0x00, 0x03, 0x00, 0x01,
  0x40, 0x00, 0x60, 0x00, 0x00, 0x03, 0x00, 0x06, 0x30, 0x00, 0x70, 0x00,
  0x00, 0x06, 0x00, 0xfc, 0x1f, 0x00, 0x30, 0x00, 0x00, 0x0e, 0x00, 0xf8,
  0x0f, 0x00, 0x38, 0x00, 0x00, 0x1c, 0x00, 0xf0, 0x07, 0x00, 0x1c, 0x00,
  0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x30, 0x00, 0x00,
  0x00, 0x00, 0x07, 0x00, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x80, 0x03, 0x00,
  0x00, 0xc0, 0x01, 0x00, 0x00, 0xc0, 0x01, 0x00, 0x00, 0x80, 0x03, 0x00,
  0x00, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x78, 0x00, 0x00,
  0x00, 0x00, 0x3c, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x00, 0xf8, 0x01,
  0xc0, 0x07, 0x00, 0x00, 0x00, 0x00, 0xc0, 0xff, 0xff, 0x01, 0x00, 0x00,
  0x00, 0x00, 0x00, 0xfe, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00
};



void sensorRead(int t)
{
  // read ultrasonic sensor
  /*
    if (t == 19 || t == 21 || u == 1) {

    DISTANCE = ultrasonic.read();
    }
  */

  // read MQ135 sensor
  if (t == 11 || t == 13 || t == 15 || u == 1) {
    MQsensorData = analogRead(MQsensorPin);
    MQsensorValue = map(MQsensorData, 0, 1023, 0, 100);
  }

  /*
    // read moisture sensor
    if (t == 3 || t == 4 || t == 6 || u == 1) {
    MoistureSensordata = analogRead(MoistureSensorPin);
    MoistureSensorValue = map(MoistureSensordata, 0, 1023, 100, 0);
    }

  */
}

// Instructions for Project
void instructions(int i)
{
  //u8g.setFont(u8g_font_6x10);
  enum {BufSize = 9}; // If a is short use a smaller number, eg 5 or 6
  //char* var = (sensor);
  char buf[BufSize];
  switch (i) {
    /*
      case 0:
      SH1106_string(0, 20, "MOISTURE SENSOR DEMO CONNECT PIN 5V -> VCC", 12, 0, oled_buf);
      break;
      case 1:
      SH1106_string(0, 20, "CONNECT PIN GND->GND", 12, 0, oled_buf);
      break;
      case 2:
      SH1106_string(0, 20, "CONNECT PIN 1.4->SIG", 12, 0, oled_buf);
      break;
      case 3:
      SH1106_string(0, 20, "GRAB THE SENSOR", 12, 0, oled_buf);
      SH1106_string(0, 40, "VALUE", 12, 0, oled_buf);
      snprintf (buf, BufSize, "%d", MoistureSensorValue);
      SH1106_string(40 , 40, buf, 12, 0, oled_buf);


      //u8g.drawStr(40, 40, buf);

      break;
      case 4:
      SH1106_string(0, 20, "RELEASE THE SENSOR", 12, 0, oled_buf);
      SH1106_string(0, 40, "VALUE", 12, 0, oled_buf);
      snprintf (buf, BufSize, "%d", MoistureSensorValue);
      SH1106_string(40 , 40, buf, 12, 0, oled_buf);
      //SH1106_char3216(40 , 40, buf, oled_buf);
      break;
      case 5:
      SH1106_string(0, 20, "DID THE VALUE CHANGE", 12, 0, oled_buf);
      break;
    */
    /*
      case 6:
      SH1106_string(0, 20, "PUT SENSOR IN SOIL", 12, 0, oled_buf);
      break;
    */
    //Gas Sensor
    case 7:
      u8g2.setCursor(0, 20);
      u8g2.print("GAS SENSOR DEMO");

      break;
    case 8:
      u8g2.setCursor(0, 20);
      u8g2.print("CONNECT PIN VCC->5V");
      break;
    case 9:
      u8g2.setCursor(0, 20);
      u8g2.print("CONNECT PIN GND->GND");
      break;
    case 10:
      u8g2.setCursor(0, 20);
      u8g2.print("CONNECT PIN 1.2->SIG");
      break;
    case 11:
      //SH1106_string(0, 20, "GRAB THE SENSOR", 12, 0, oled_buf);
      u8g2.setCursor(0, 20);
      u8g2.print("VALUE ");
      u8g2.setCursor(40, 40);
      u8g2.print(MQsensorValue);
      break;
    case 12:
      u8g2.setCursor(0, 20);
      u8g2.print("BRING PERFUMED COTTON");
      u8g2.setCursor(0, 40);
      u8g2.print("NEAR SENSOR");
      break;
    case 13:
      //SH1106_string(0, 20, "RELEASE THE SENSOR", 12, 0, oled_buf);
      u8g2.setCursor(0, 20);
      u8g2.print("VALUE = ");
      u8g2.setCursor(50, 20);
      u8g2.print(MQsensorValue);

      if (MQsensorValue > 50) {
        u8g2.setCursor(0, 40);
        u8g2.print("ALCOHOL DETECTED");

      }
      //SH1106_char3216(40 , 40, buf, oled_buf);
      break;

    case 14:

      u8g2.setCursor(0, 20);
      u8g2.print("DID THE VALUE CHANGE");
      break;
      /*
          case 16:
            {
              if (analogRead(PulseSensorPurplePin) >= 520)
                one = millis();
              while (analogRead(PulseSensorPurplePin) >= 520);
              while (analogRead(PulseSensorPurplePin) < 520);
              two = millis();
              three = abs(two - one);
              if (three > 240 && three < 2400)
              {
                temp = 60000 / (three);
                accu += temp;
                i++;
                if (i == 50) {
                  i = 1;
                  accu = temp;
                }
              }
              if (i == 49)
              {
                u8g2.setCursor(40, 40);
                u8g2.print(accu / i);
              }
            }
            break;*/
      /*
        // Ultrasonic sensor
        case 16:
        SH1106_string(0, 20, "ULTRASONIC SENSOR    DEMO", 12, 0, oled_buf);
        break;
        case 17:
        SH1106_string(0, 20, "CONNECT PIN VCC->5V", 12, 0, oled_buf);
        break;
        case 18:
        SH1106_string(0, 20, "CONNECT PIN GND->GND", 12, 0, oled_buf);
        break;
        case 19:
        SH1106_string(0, 20, "CONNECT PIN 4.6->TRIG", 12, 0, oled_buf);
        break;
        case 20:
        SH1106_string(0, 20, "CONNECT PIN 4.5->ECHO", 12, 0, oled_buf);
        break;
        case 21:
        //SH1106_string(0, 20, "GRAB THE SENSOR", 12, 0, oled_buf);
        SH1106_string(0, 40, "DISTANCE", 12, 0, oled_buf);
        snprintf (buf, BufSize, "%d", DISTANCE);
        SH1106_string(60, 40, buf, 12, 0, oled_buf);
        break;
        case 22:
        SH1106_string(0, 20, "BRING YOUR HAND CLOSE TO THE SENSOR", 12, 0, oled_buf);
        break;
        case 23:
        //SH1106_string(0, 20, "RELEASE THE SENSOR", 12, 0, oled_buf);
        SH1106_string(0, 40, "DISTANCE", 12, 0, oled_buf);
        snprintf (buf, BufSize, "%d", DISTANCE);
        SH1106_string(60 , 40, buf, 12, 0, oled_buf);
        //SH1106_char3216(40 , 40, buf, oled_buf);
        break;
      */
      /*
        case 16:
        SH1106_string(0, 20, "DO YOU WANT TO UPLOADTHIS DATA TO THE     CLOUD", 12, 0, oled_buf);
        break;

        case 19:
        SH1106_clear(oled_buf);
        SH1106_string(0, 20, "UPLOADING COMPLETED", 12, 0, oled_buf);
        break;
      */
  }
}



//uint8_t pin_up = A0;
//uint8_t pin_down = A3;
//uint8_t pin_fire = 7;

const int PulseSensorPurplePin = A0;
int Threshold = 2060;
int temp, j, accu = 0;
unsigned long one, two, three;

//.......................Joystick for shooting and movement
const int SW_pin = P2_2;  // digital pin connected to switch output
const int X_High = 3272, X_Low = 919, Y_High = 3272, Y_Low = 919;
const uint8_t Y_pin = P1_3;
const uint8_t X_pin = A1;
int BPMValue = 90;

int k = 50, i = 1;

int fire()
{
  if (analogRead(Y_pin) > Y_High)
    return 1;
  else
    return 0;
}

#define ST_FP 4

/* object types */
struct _st_ot_struct
{
  /*
    missle and hit:
      bit 0: player missle and trash
      bit 1: trash, which might hit the player
  */

  uint8_t missle_mask;    /* this object is a missle: it might destroy something if the target is_hit_fn says so */
  uint8_t hit_mask;     /* if missle_mask & hit_mask is != 0  then the object can be destroyed */
  uint8_t points;
  uint8_t draw_fn;
  uint8_t move_fn;
  /* ST_MOVE_FN_NONE, ST_MOVE_FN_X_SLOW */
  uint8_t destroy_fn;   /* object can be destroyed by a missle (e.g. a missle from the space ship) */
  /* ST_DESTROY_FN_NONE, ST_DESTROY_FN_SPLIT */
  uint8_t is_hit_fn;    /* is hit procedure */
  /* ST_IS_HIT_FN_NONE, ST_IS_HIT_BBOX */
  uint8_t fire_fn;
  /* ST_FIRE_FN_NONE, ST_FIRE_FN_X_LEFT */

};
typedef struct _st_ot_struct st_ot;

/*
  objects, which are visible at the play area
*/
struct _st_obj_struct
{
  uint8_t ot;       /* object type: zero means, object is not used */
  int8_t tmp;     /* generic value, used by ST_MOVE_IMPLODE */
  /* absolute position */
  /* LCD pixel position is x>>ST_FP and y>>ST_FP */
  int16_t x, y;
  int8_t x0, y0, x1, y1; /* object outline in pixel, reference point is at 0,0 */
};
typedef struct _st_obj_struct st_obj;

#define ST_DRAW_NONE 0
#define ST_DRAW_BBOX 1
#define ST_DRAW_TRASH1 2
#define ST_DRAW_PLAYER1 3
#define ST_DRAW_TRASH2 4
#define ST_DRAW_PLAYER2 5
#define ST_DRAW_PLAYER3 6
#define ST_DRAW_GADGET 7
#define ST_DRAW_BACKSLASH 8
#define ST_DRAW_SLASH 9
#define ST_DRAW_BIG_TRASH 10

#define ST_MOVE_NONE 0
#define ST_MOVE_X_SLOW 1
#define ST_MOVE_PX_NORMAL 2
#define ST_MOVE_PX_FAST 3
#define ST_MOVE_PLAYER 4
#define ST_MOVE_PY 5
#define ST_MOVE_NY 6
#define ST_MOVE_IMPLODE 7
#define ST_MOVE_X_FAST 8
#define ST_MOVE_WALL 9
#define ST_MOVE_NXPY 10
#define ST_MOVE_NXNY 11

#define ST_IS_HIT_NONE 0
#define ST_IS_HIT_BBOX 1
#define ST_IS_HIT_WALL 2

#define ST_DESTROY_NONE 0
#define ST_DESTROY_DISAPPEAR 1
#define ST_DESTROY_TO_DUST 2
#define ST_DESTROY_GADGET 3
#define ST_DESTROY_PLAYER 4
#define ST_DESTROY_PLAYER_GADGETS 5
#define ST_DESTROY_BIG_TRASH 6

#define ST_FIRE_NONE 0
#define ST_FIRE_PLAYER1 1
#define ST_FIRE_PLAYER2 2
#define ST_FIRE_PLAYER3 3

#define ST_OT_WALL_SOLID 1
#define ST_OT_BIG_TRASH 2
#define ST_OT_MISSLE 3
#define ST_OT_TRASH1 4
#define ST_OT_PLAYER 5
#define ST_OT_DUST_PY 6
#define ST_OT_DUST_NY 7
#define ST_OT_TRASH_IMPLODE 8
#define ST_OT_TRASH2 9
#define ST_OT_PLAYER2 10
#define ST_OT_PLAYER3 11
#define ST_OT_GADGET 12
#define ST_OT_GADGET_IMPLODE 13
#define ST_OT_DUST_NXPY 14
#define ST_OT_DUST_NXNY 15


/*================================================================*/
/* graphics object */
/*================================================================*/

u8g2_t *st_u8g2;

u8g2_uint_t u8g_height_minus_one;


#define ST_AREA_HEIGHT (st_u8g2->height - 8)
#define ST_AREA_WIDTH (st_u8g2->width)


/*================================================================*/
/* object types */
/*================================================================*/


const st_ot st_object_types[] U8X8_PROGMEM =
{
  /* 0: empty object type */
  { 0, 0,  0, ST_DRAW_NONE, ST_MOVE_NONE, ST_DESTROY_DISAPPEAR, ST_IS_HIT_NONE, ST_FIRE_NONE },
  /* 1: wall, player will be destroyed */
  { 2, 1, 30, ST_DRAW_BBOX, ST_MOVE_WALL, ST_DESTROY_DISAPPEAR, ST_IS_HIT_WALL, ST_FIRE_NONE },
  /* ST_OT_BIG_TRASH (2) */
  { 2, 1,  0, ST_DRAW_BIG_TRASH, ST_MOVE_X_SLOW, ST_DESTROY_BIG_TRASH, ST_IS_HIT_BBOX, ST_FIRE_NONE },
  /* 3: simple space ship (player) missle */
  { 1, 0,  0, ST_DRAW_BBOX, ST_MOVE_PX_FAST, ST_DESTROY_DISAPPEAR, ST_IS_HIT_NONE, ST_FIRE_NONE },
  /* ST_OT_TRASH1 (4): trash */
  { 2, 1,  0, ST_DRAW_TRASH1, ST_MOVE_X_SLOW, ST_DESTROY_TO_DUST, ST_IS_HIT_BBOX, ST_FIRE_NONE },
  /* ST_OT_PLAYER (5): player space ship */
  { 0, 2,  0, ST_DRAW_PLAYER1, ST_MOVE_PLAYER, ST_DESTROY_PLAYER, ST_IS_HIT_BBOX, ST_FIRE_PLAYER1 },
  /* ST_OT_DUST_PY (6): Last part of trash  */
  { 0, 0,  0, ST_DRAW_BBOX, ST_MOVE_PY, ST_DESTROY_NONE, ST_IS_HIT_NONE, ST_FIRE_NONE },
  /* ST_OT_DUST_NY (7): Last part of trash  */
  { 0, 0,  0, ST_DRAW_BBOX, ST_MOVE_NY, ST_DESTROY_NONE, ST_IS_HIT_NONE, ST_FIRE_NONE },
  /* ST_OT_TRASH_IMPLODE (8): trash was hit */
  { 0, 0,  5, ST_DRAW_TRASH1, ST_MOVE_IMPLODE, ST_DESTROY_NONE, ST_IS_HIT_NONE, ST_FIRE_NONE },
  /* ST_OT_TRASH2 (9): trash */
  { 2, 1,  0, ST_DRAW_TRASH2, ST_MOVE_X_SLOW, ST_DESTROY_TO_DUST, ST_IS_HIT_BBOX, ST_FIRE_NONE },
  /* ST_OT_PLAYER2 (10): player space ship+1x enhancement */
  { 0, 2,  0, ST_DRAW_PLAYER2, ST_MOVE_PLAYER, ST_DESTROY_PLAYER_GADGETS, ST_IS_HIT_BBOX, ST_FIRE_PLAYER2 },
  /* ST_OT_PLAYER3 (11): player space ship+2x enhancement */
  { 0, 2,  0, ST_DRAW_PLAYER3, ST_MOVE_PLAYER, ST_DESTROY_PLAYER_GADGETS, ST_IS_HIT_BBOX, ST_FIRE_PLAYER3 },
  /* ST_OT_GADGET (12): adds enhancements */
  { 0, 1,  0, ST_DRAW_GADGET, ST_MOVE_X_FAST, ST_DESTROY_GADGET, ST_IS_HIT_BBOX, ST_FIRE_NONE },
  /* ST_OT_GADGET_IMPLODE (13) */
  { 0, 0, 20, ST_DRAW_GADGET, ST_MOVE_IMPLODE, ST_DESTROY_NONE, ST_IS_HIT_NONE, ST_FIRE_NONE },
  /* ST_OT_DUST_NXPY (14): Last part of trash  */
  { 0, 0,  0, ST_DRAW_BACKSLASH, ST_MOVE_NXPY, ST_DESTROY_NONE, ST_IS_HIT_NONE, ST_FIRE_NONE },
  /* ST_OT_DUST_NXNY (15): Last part of trash  */
  { 0, 0,  0, ST_DRAW_SLASH, ST_MOVE_NXNY, ST_DESTROY_NONE, ST_IS_HIT_NONE, ST_FIRE_NONE },

};

/*================================================================*/
/* list of all objects on the screen */
/*================================================================*/

/* use AVR RAMEND constant to derive the number of allowed objects */

#if RAMEND < 0x300
#define ST_OBJ_CNT 25
#else
//#define ST_OBJ_CNT 45
#define ST_OBJ_CNT 60
#endif

st_obj st_objects[ST_OBJ_CNT];

/*================================================================*/
/* about players space ship*/
/*================================================================*/

/* player position */
uint8_t st_player_pos;

/* points */
#define ST_POINTS_PER_LEVEL 0
uint16_t st_player_points;
uint16_t st_player_points_delayed;
uint16_t st_highscore = 0;

/*================================================================*/
/* overall game state */
/*================================================================*/

#define ST_STATE_PREPARE 0
#define ST_STATE_IPREPARE 1
#define ST_STATE_GAME 2
#define ST_STATE_END 3
#define ST_STATE_IEND 4

uint8_t st_state = ST_STATE_PREPARE;

/*================================================================*/
/* game difficulty */
/*================================================================*/
uint8_t st_difficulty = 1;
#define ST_DIFF_VIS_LEN 30
#define ST_DIFF_FP 5
uint16_t st_to_diff_cnt = 0;

/*================================================================*/
/* bitmaps */
/*================================================================*/

const uint8_t st_bitmap_player1[]  =
{
  /* 01100000 */ 0x060,
  /* 11111000 */ 0x0f8,
  /* 01111110 */ 0x07e,
  /* 11111000 */ 0x0f8,
  /* 01100000 */ 0x060
};

const uint8_t st_bitmap_player2[] =
{
  /* 01100000 */ 0x060,
  /* 01111100 */ 0x078,
  /* 01100000 */ 0x060,
  /* 11100000 */ 0x0e0,
  /* 11111000 */ 0x0f8,
  /* 01111110 */ 0x07e,
  /* 11111000 */ 0x0f8,
  /* 01100000 */ 0x060
};

const uint8_t st_bitmap_player3[] =
{
  /* 01100000 */ 0x060,
  /* 01111100 */ 0x078,
  /* 01100000 */ 0x060,
  /* 11100000 */ 0x0e0,
  /* 11111000 */ 0x0f8,
  /* 01111110 */ 0x07e,
  /* 11111000 */ 0x0f8,
  /* 11100000 */ 0x0e0,
  /* 01100000 */ 0x060,
  /* 01111100 */ 0x078,
  /* 01100000 */ 0x060
};

const uint8_t st_bitmap_trash_5x5_1[] =
{
  /* 01110000 */ 0x070,
  /* 11110000 */ 0x0f0,
  /* 11111000 */ 0x0f8,
  /* 01111000 */ 0x078,
  /* 00110000 */ 0x030,
};

const uint8_t st_bitmap_trash_5x5_2[] =
{
  /* 00110000 */ 0x030,
  /* 11111000 */ 0x0f8,
  /* 11111000 */ 0x0f8,
  /* 11110000 */ 0x0f0,
  /* 01110000 */ 0x070,
};

const uint8_t st_bitmap_trash_7x7[] =
{
  /* 00111000 */ 0x038,
  /* 01111100 */ 0x07c,
  /* 11111100 */ 0x0fc,
  /* 11111110 */ 0x0fe,
  /* 11111110 */ 0x0fe,
  /* 01111110 */ 0x07e,
  /* 01111000 */ 0x078,
};

const uint8_t st_bitmap_gadget[] =
{
  /* 01110000 */ 0x070,
  /* 11011000 */ 0x0d8,
  /* 10001000 */ 0x088,
  /* 11011000 */ 0x0d8,
  /* 01110000 */ 0x070,
};

/*================================================================*/
/* forward definitions */
/*================================================================*/
uint8_t st_rnd(void) U8X8_NOINLINE;
static st_obj *st_GetObj(uint8_t objnr) U8X8_NOINLINE;
uint8_t st_GetMissleMask(uint8_t objnr);
uint8_t st_GetHitMask(uint8_t objnr);
int8_t st_FindObj(uint8_t ot) U8X8_NOINLINE;
void st_ClrObjs(void) U8X8_NOINLINE;
int8_t st_NewObj(void) U8X8_NOINLINE;
uint8_t st_CntObj(uint8_t ot);
uint8_t st_CalcXY(st_obj *o) U8X8_NOINLINE;
void st_SetXY(st_obj *o, uint8_t x, uint8_t y) U8X8_NOINLINE;

void st_FireStep(uint8_t is_auto_fire, uint8_t is_fire) U8X8_NOINLINE;

void st_InitTrash(uint8_t x, uint8_t y, int8_t dir);
void st_NewGadget(uint8_t x, uint8_t y);
void st_NewPlayerMissle(uint8_t x, uint8_t y) ;
void st_NewTrashDust(uint8_t x, uint8_t y, int ot);
void st_NewTrashDustAreaArgs(int16_t x, int16_t y, int ot);
void st_SetupPlayer(uint8_t objnr, uint8_t ot);


/*================================================================*/
/* utility functions */
/*================================================================*/

char st_itoa_buf[12];
char *st_itoa(unsigned long v)
{
  volatile unsigned char i = 11;
  st_itoa_buf[11] = '\0';
  while ( i > 0)
  {
    i--;
    st_itoa_buf[i] = (v % 10) + '0';
    v /= 10;
    if ( v == 0 )
      break;
  }
  return st_itoa_buf + i;
}


uint8_t st_rnd(void)
{
  return rand();
}

/*
  for the specified index number, return the object
*/
static st_obj *st_GetObj(uint8_t objnr)
{
  return st_objects + objnr;
}


/*
  check, if this is a missle-like object (that is, can this object destroy something else)
*/
uint8_t st_GetMissleMask(uint8_t objnr)
{
  st_obj *o = st_GetObj(objnr);
  return u8x8_pgm_read(&(st_object_types[o->ot].missle_mask));
}

/*
  check, if this is a missle-like object (that is, can this object destroy something else)
*/
uint8_t st_GetHitMask(uint8_t objnr)
{
  st_obj *o = st_GetObj(objnr);
  return u8x8_pgm_read(&(st_object_types[o->ot].hit_mask));
}

/*
  search an empty object
*/
int8_t st_FindObj(uint8_t ot)
{
  int8_t i;
  for ( i = 0; i < ST_OBJ_CNT; i++ )
  {
    if ( st_objects[i].ot == ot )
      return i;
  }
  return -1;
}

/*
  delete all objects
*/

void st_ClrObjs(void)
{
  int8_t i;
  for ( i = 0; i < ST_OBJ_CNT; i++ )
    st_objects[i].ot = 0;
}

/*
  search an empty object
*/
int8_t st_NewObj(void)
{
  int8_t i;
  for ( i = 0; i < ST_OBJ_CNT; i++ )
  {
    if ( st_objects[i].ot == 0 )
      return i;
  }
  return -1;
}

/*
  count number of objectes of the provided type
  st_CntObj(0) will return the number of empty objects, that means if
  st_CntObj(0) > 0 then st_NewObj() will return  a valid index
*/
uint8_t st_CntObj(uint8_t ot)
{
  uint8_t i;
  uint8_t cnt = 0;
  for ( i = 0; i < ST_OBJ_CNT; i++ )
  {
    if ( st_objects[i].ot == ot )
      cnt++;
  }
  return cnt;
}

/*
  calculate the pixel coordinates of the reference point of an object
  return rhe x value
*/
uint8_t st_px_x, st_px_y; /* pixel within area */
uint8_t st_CalcXY(st_obj *o)
{
  //st_obj *o = st_GetObj(objnr);
  st_px_y = o->y >> ST_FP;
  st_px_x = o->x >> ST_FP;
  return st_px_x;
}

void st_SetXY(st_obj *o, uint8_t x, uint8_t y)
{
  o->x = ((int16_t)x) << ST_FP;
  o->y = ((int16_t)y) << ST_FP;
}

/*
  calculate the object bounding box and place it into some global variables
*/
int16_t st_bbox_x0, st_bbox_y0, st_bbox_x1, st_bbox_y1;

void st_CalcBBOX(uint8_t objnr)
{
  st_obj *o = st_GetObj(objnr);

  st_bbox_x0 = (uint16_t)(o->x >> ST_FP);
  st_bbox_x1 = st_bbox_x0;
  st_bbox_x0 += o->x0;
  st_bbox_x1 += o->x1;

  st_bbox_y0 = (uint16_t)(o->y >> ST_FP);
  st_bbox_y1 = st_bbox_y0;
  st_bbox_y0 += o->y0;
  st_bbox_y1 += o->y1;
}

/*
  clip bbox with the view window. requires a call to st_CalcBBOX
  return 0, if the bbox is totally outside the window
*/
uint8_t st_cbbox_x0, st_cbbox_y0, st_cbbox_x1, st_cbbox_y1;
uint8_t st_ClipBBOX(void)
{
  if ( st_bbox_x0 >= ST_AREA_WIDTH )
    return 0;
  if ( st_bbox_x0 >= 0 )
    st_cbbox_x0  = (uint16_t)st_bbox_x0;
  else
    st_cbbox_x0 = 0;

  if ( st_bbox_x1 < 0 )
    return 0;
  if ( st_bbox_x1 < ST_AREA_WIDTH )
    st_cbbox_x1  = (uint16_t)st_bbox_x1;
  else
    st_cbbox_x1 = ST_AREA_WIDTH - 1;

  if ( st_bbox_y0 >= ST_AREA_HEIGHT )
    return 0;
  if ( st_bbox_y0 >= 0 )
    st_cbbox_y0  = (uint16_t)st_bbox_y0;
  else
    st_cbbox_y0 = 0;

  if ( st_bbox_y1 < 0 )
    return 0;
  if ( st_bbox_y1 < ST_AREA_HEIGHT )
    st_cbbox_y1  = (uint16_t)st_bbox_y1;
  else
    st_cbbox_y1 = ST_AREA_HEIGHT - 1;

  return 1;
}


/*================================================================*/
/* universal member functions */
/*================================================================*/


uint8_t st_IsOut(uint8_t objnr)
{
  st_CalcBBOX(objnr);
  if ( st_bbox_x0 >= ST_AREA_WIDTH )
    return 1;
  if ( st_bbox_x1 < 0 )
    return 1;
  if ( st_bbox_y0 >= ST_AREA_HEIGHT )
    return 1;
  if ( st_bbox_y1 < 0 )
    return 1;
  return 0;
}

void st_Disappear(uint8_t objnr)
{
  st_obj *o = st_GetObj(objnr);
  st_player_points += u8x8_pgm_read(&(st_object_types[o->ot].points));
  o->ot = 0;
}

/*================================================================*/
/* type dependent member functions */
/*================================================================*/

void st_Move(uint8_t objnr)
{
  st_obj *o = st_GetObj(objnr);
  switch (u8x8_pgm_read(&(st_object_types[o->ot].move_fn)))
  {
    case ST_MOVE_NONE:
      break;
    case ST_MOVE_X_SLOW:
      o->x -= (1 << ST_FP) / 8;
      o->x -= st_difficulty;
      o->y += (int16_t)o->tmp;
      if ( o->y >= ((ST_AREA_HEIGHT - 1) << ST_FP) || o->y <= 0 )
        o->tmp = - o->tmp;
      break;
    case ST_MOVE_X_FAST:
      o->x -= (1 << ST_FP) / 2;
      o->y += (int16_t)o->tmp;
      if ( o->y >= ((ST_AREA_HEIGHT - 1) << ST_FP) || o->y <= 0 )
        o->tmp = - o->tmp;
      break;
    case ST_MOVE_PX_NORMAL:
      o->x += (1 << ST_FP) / 4;
      break;
    case ST_MOVE_PX_FAST:
      o->x += (1 << ST_FP);
      break;
    case ST_MOVE_PLAYER:
      o->y = st_player_pos << ST_FP;
      break;
    case ST_MOVE_PY:
      o->y += 3 * ST_FP;
      break;
    case ST_MOVE_NY:
      o->y -= 3 * ST_FP;
      break;
    case ST_MOVE_NXPY:
      o->y += 3 * ST_FP;
      o->x -= 3 * ST_FP;
      break;
    case ST_MOVE_NXNY:
      o->y -= 3 * ST_FP;
      o->x -= 3 * ST_FP;
      break;
    case ST_MOVE_IMPLODE:
      o->tmp++;
      if ( (o->tmp & 0x03) == 0 )
      {
        if ( o->x0 != o->x1 )
          o->x0++;
        else
          st_Disappear(objnr);
      }
      break;
    case ST_MOVE_WALL:
      o->x -= 1;
      o->x -= (st_difficulty >> 1);
      break;
  }
}

void st_DrawBBOX(uint8_t objnr)
{
  uint8_t y0, y1;
  /*st_obj *o = st_GetObj(objnr);*/
  st_CalcBBOX(objnr);
  if ( st_ClipBBOX() == 0 )
    return;
  /* st_cbbox_x0, st_cbbox_y0, st_cbbox_x1, st_cbbox_y1; */


  // w = st_cbbox_x1-st_cbbox_x0;
  // w++;
  // h = st_cbbox_y1-st_cbbox_y0;
  // h++;


  //dog_SetVLine(st_cbbox_x0, st_cbbox_y0, st_cbbox_y1);
  //dog_SetVLine(st_cbbox_x1, st_cbbox_y0, st_cbbox_y1);
  //dog_SetHLine(st_cbbox_x0, st_cbbox_x1, st_cbbox_y0);
  //dog_SetHLine(st_cbbox_x0, st_cbbox_x1, st_cbbox_y1);

  u8g2_SetDrawColor(st_u8g2, 1);
  y0 = u8g_height_minus_one - st_cbbox_y0;
  y1 = u8g_height_minus_one - st_cbbox_y1;

  u8g2_DrawFrame(st_u8g2, st_cbbox_x0, y1, st_cbbox_x1 - st_cbbox_x0 + 1, y0 - y1 + 1);

  //dog_SetBox(st_cbbox_x0, st_cbbox_y0, st_cbbox_x1, st_cbbox_y1);

  /*
    if ( o->ot == ST_OT_PLAYER )
    {
    dog_DrawStr(0, 26, font_4x6, st_itoa(st_cbbox_y0));
    dog_DrawStr(10, 26, font_4x6, st_itoa(st_cbbox_y1));
    }
  */
}

#ifdef FN_IS_NOT_IN_USE
void st_DrawFilledBox(uint8_t objnr)
{
  st_CalcBBOX(objnr);
  if ( st_ClipBBOX() == 0 )
    return;
  /* st_cbbox_x0, st_cbbox_y0, st_cbbox_x1, st_cbbox_y1; */
  dog_SetBox(st_cbbox_x0, st_cbbox_y0, st_cbbox_x1, st_cbbox_y1);
}
#endif

void st_DrawBitmap(uint8_t objnr, const uint8_t * bm, uint8_t w, uint8_t h)
{
  /* st_obj *o = st_GetObj(objnr); */
  st_CalcBBOX(objnr);
  /* result is here: int16_t st_bbox_x0, st_bbox_y0, st_bbox_x1, st_bbox_y1 */
  //dog_SetBitmapP(st_bbox_x0,st_bbox_y1,bm,w,h);

  u8g2_DrawBitmap(st_u8g2, st_bbox_x0, u8g_height_minus_one - st_bbox_y1, (w + 7) / 8, h, bm);

}

void st_DrawObj(uint8_t objnr)
{
  st_obj *o = st_GetObj(objnr);
  switch (u8x8_pgm_read(&(st_object_types[o->ot].draw_fn)))
  {
    case ST_DRAW_NONE:
      break;
    case ST_DRAW_BBOX:
      st_DrawBBOX(objnr);
      break;
    case ST_DRAW_TRASH1:
      st_DrawBitmap(objnr, st_bitmap_trash_5x5_1, o->x1 - o->x0 + 1, 5);
      break;
    case ST_DRAW_TRASH2:
      st_DrawBitmap(objnr, st_bitmap_trash_5x5_2, o->x1 - o->x0 + 1, 5);
      break;
    case ST_DRAW_BIG_TRASH:
      st_DrawBitmap(objnr, st_bitmap_trash_7x7, o->x1 - o->x0 + 1, 7);
      break;
    case ST_DRAW_PLAYER1:
      st_DrawBitmap(objnr, st_bitmap_player1, 7, 5);
      break;
    case ST_DRAW_PLAYER2:
      st_DrawBitmap(objnr, st_bitmap_player2, 7, 8);
      break;
    case ST_DRAW_PLAYER3:
      st_DrawBitmap(objnr, st_bitmap_player3, 7, 11);
      break;
    case ST_DRAW_GADGET:
      /* could use this proc, but... */
      /* st_DrawBitmap(objnr, st_bitmap_gadget,o->x1-o->x0+1, 5); */
      /* ... this one looks also funny. */
      st_DrawBitmap(objnr, st_bitmap_gadget, 5, 5);
      break;
    case ST_DRAW_BACKSLASH:
      {
        uint8_t x;
        uint8_t y;
        x = st_CalcXY(o);
        y = st_px_y;


        // dog_SetPixel(x,y);
        // x++; y--;
        // dog_SetPixel(x,y);
        // x++; y--;
        // dog_SetPixel(x,y);

        u8g2_SetDrawColor(st_u8g2, 1);
        u8g2_DrawPixel(st_u8g2, x, u8g_height_minus_one - y);
        x++; y--;
        u8g2_DrawPixel(st_u8g2, x, u8g_height_minus_one - y);
        x++; y--;
        u8g2_DrawPixel(st_u8g2, x, u8g_height_minus_one - y);
      }
      break;
    case ST_DRAW_SLASH:
      {
        uint8_t x;
        uint8_t y;
        x = st_CalcXY(o);
        y = st_px_y;

        // dog_SetPixel(x,y);
        // x++; y++;
        // dog_SetPixel(x,y);
        // x++; y++;
        // dog_SetPixel(x,y);

        u8g2_SetDrawColor(st_u8g2, 1);
        u8g2_DrawPixel(st_u8g2, x, u8g_height_minus_one - y);
        x++; y++;
        u8g2_DrawPixel(st_u8g2, x, u8g_height_minus_one - y);
        x++; y++;
        u8g2_DrawPixel(st_u8g2, x, u8g_height_minus_one - y);
      }
      break;
  }
}

uint8_t st_IsHitBBOX(uint8_t objnr, uint8_t x, uint8_t y)
{
  st_CalcBBOX(objnr);
  if ( st_ClipBBOX() == 0 )
    return 0; /* obj is outside (not visible) */
  if ( x < st_cbbox_x0 )
    return 0;
  if ( x > st_cbbox_x1 )
    return 0;
  if ( y < st_cbbox_y0 )
    return 0;
  if ( y > st_cbbox_y1 )
    return 0;
  return 1;
}

void st_Destroy(uint8_t objnr)
{
  int8_t nr;
  st_obj *o = st_GetObj(objnr);
  switch (u8x8_pgm_read(&(st_object_types[o->ot].destroy_fn)))
  {
    case ST_DESTROY_NONE:      /* only usefull for missels or walls which stay alife */
      break;
    case ST_DESTROY_DISAPPEAR:  /* this should be the default operation */
      st_Disappear(objnr);
      break;
    case ST_DESTROY_GADGET:
      nr = st_FindObj(ST_OT_PLAYER2);
      if ( nr >= 0 )
        st_SetupPlayer(nr, ST_OT_PLAYER3);
      else
      {
        nr = st_FindObj(ST_OT_PLAYER);
        if ( nr >= 0 )
          st_SetupPlayer(nr, ST_OT_PLAYER2);
      }
      st_NewTrashDustAreaArgs(o->x, o->y, ST_OT_DUST_PY);
      st_NewTrashDustAreaArgs(o->x, o->y, ST_OT_DUST_NY);
      o->ot = ST_OT_GADGET_IMPLODE;
      o->tmp = 0;
      break;
    case ST_DESTROY_TO_DUST:
      st_NewTrashDustAreaArgs(o->x, o->y, ST_OT_DUST_PY);
      st_NewTrashDustAreaArgs(o->x, o->y, ST_OT_DUST_NY);
      o->ot = ST_OT_TRASH_IMPLODE;
      o->tmp = 0;
      break;
    case ST_DESTROY_BIG_TRASH:
      st_NewTrashDustAreaArgs(o->x, o->y, ST_OT_DUST_PY);
      st_NewTrashDustAreaArgs(o->x, o->y, ST_OT_DUST_NY);
      st_InitTrash((o->x >> ST_FP) - 1, (o->y >> ST_FP) + 3, 2 + (st_rnd() & 3));
      st_InitTrash((o->x >> ST_FP) - 2, (o->y >> ST_FP) - 3, -2 - (st_rnd() & 3));
      st_Disappear(objnr);
      break;
    case ST_DESTROY_PLAYER:
      st_Disappear(objnr);
      st_state = ST_STATE_END;
      o->tmp = 0;
      break;
    case ST_DESTROY_PLAYER_GADGETS:
      /* o->ot = ST_OT_PLAYER; */
      st_SetupPlayer(objnr, ST_OT_PLAYER);
      break;
  }
}

/*
  check if the target (objnr) has been hit.
  st_IsHit() must also destroy the target.
  return value:
    0: do not destroy the missle
    1: destroy the missle
*/
uint8_t st_IsHit(uint8_t objnr, uint8_t x, uint8_t y, uint8_t missle_mask)
{
  uint8_t hit_mask = st_GetHitMask(objnr);
  st_obj *o;

  if ( (hit_mask & missle_mask) == 0 )
    return 0;

  o = st_GetObj(objnr);

  switch (u8x8_pgm_read(&(st_object_types[o->ot].is_hit_fn)))
  {
    case ST_IS_HIT_NONE:
      break;
    case ST_IS_HIT_BBOX:
      if ( st_IsHitBBOX(objnr, x, y) != 0 )
      {
        st_Destroy(objnr);
        return 1;
      }
      break;
    case ST_IS_HIT_WALL:
      if ( st_IsHitBBOX(objnr, x, y) != 0 )
      {
        o->x0++;
        if ( o->x0 < o->x1 )
        {
          st_NewTrashDust(x, y, ST_OT_DUST_NXPY);
          st_NewTrashDust(x, y, ST_OT_DUST_NXNY);
        }
        else
        {
          st_Destroy(objnr);
          st_NewTrashDust(x, y, ST_OT_DUST_NXPY);
          st_NewTrashDust(x, y, ST_OT_DUST_NXNY);
          st_NewTrashDust(x, y, ST_OT_DUST_NY);
          st_NewTrashDust(x, y, ST_OT_DUST_PY);
        }
        return 1;
      }
      break;
  }
  return 0;
}



/* update all fire counters */
uint8_t st_fire_player = 0;
uint8_t st_fire_period = 21;
uint8_t st_manual_fire_delay = 20;
uint8_t st_is_fire_last_value = 0;

/*
  is_auto_fire == 1
    is_fire will be ignored, autofire enabled
  is_auto_fire == 0
    a transition from 1 to 0 on the is_fire variable will issue a missle
*/
void st_FireStep(uint8_t is_auto_fire, uint8_t is_fire)
{
  if ( is_auto_fire != 0 )
  {
    st_fire_player++;
    if ( st_fire_player >= st_fire_period )
      st_fire_player = 0;
  }
  else
  {
    if ( st_fire_player < st_manual_fire_delay )
    {
      st_fire_player++;
    }
    else
    {
      if ( st_is_fire_last_value == 0 )
        if ( is_fire != 0 )
          st_fire_player = 0;
    }
    st_is_fire_last_value = is_fire;
  }
}

void st_Fire(uint8_t objnr)
{
  st_obj *o = st_GetObj(objnr);
  uint8_t x;
  uint8_t y;

  switch (u8x8_pgm_read(&(st_object_types[o->ot].fire_fn)))
  {
    case ST_FIRE_NONE:
      break;
    case ST_FIRE_PLAYER1:
      if ( st_fire_player == 0 )
      {
        /* create missle at st_px_x and st_px_y */
        x = st_CalcXY(o);
        y = st_px_y;
        st_NewPlayerMissle(x , y );
      }
      break;
    case ST_FIRE_PLAYER2:
      if ( st_fire_player == 0 )
      {
        /* create missle at st_px_x and st_px_y */
        x = st_CalcXY(o);
        y = st_px_y;
        st_NewPlayerMissle(x , y );
        st_NewPlayerMissle(x , y + 4 );
      }
      break;
    case ST_FIRE_PLAYER3:
      if ( st_fire_player == 0 )
      {
        /* create missle at st_px_x and st_px_y */
        x = st_CalcXY(o);
        y = st_px_y;
        st_NewPlayerMissle(x , y );
        st_NewPlayerMissle(x , y + 4 );
        st_NewPlayerMissle(x , y - 4 );
      }
      break;
  }
}

/*================================================================*/
/* object init functions */
/*================================================================*/

/*
  x,y are pixel coordinats within the play arey
*/
void st_NewGadget(uint8_t x, uint8_t y)
{
  st_obj *o;
  int8_t objnr = st_NewObj();
  if ( objnr < 0 )
    return;
  o = st_GetObj(objnr);
  st_SetXY(o, x, y);
  o->ot = ST_OT_GADGET;
  o->tmp = 8;
  //o->x = (x)<<ST_FP;
  //o->y = (y)<<ST_FP;
  o->x0 = -3;
  o->x1 = 1;
  o->y0 = -2;
  o->y1 = 2;
}

/*
  x,y are pixel coordinats within the play arey
  dir: direction
    0: random
   != 0 --> assigned
*/
void st_InitTrash(uint8_t x, uint8_t y, int8_t dir)
{
  st_obj *o;
  int8_t objnr = st_NewObj();
  if ( objnr < 0 )
    return;
  o = st_GetObj(objnr);
  if ( (st_rnd() & 1) == 0 )
    o->ot = ST_OT_TRASH1;
  else
    o->ot = ST_OT_TRASH2;
  if ( dir == 0 )
  {
    o->tmp = 0;
    if ( st_rnd() & 1 )
    {
      if ( st_rnd() & 1 )
        o->tmp++;
      else
        o->tmp--;
    }
  }
  else
  {
    o->tmp = dir;
  }
  st_SetXY(o, x, y);
  //o->x = (x)<<ST_FP;
  //o->y = (y)<<ST_FP;
  o->x0 = -3;
  o->x1 = 1;
  o->y0 = -2;
  o->y1 = 2;
  if ( st_difficulty >= 5 )
  {
    if ( (st_rnd() & 3) == 0 )
    {
      o->ot = ST_OT_BIG_TRASH;
      o->y0--;
      o->y1++;
      o->x0--;
      o->x1++;
    }
  }
}

/*
  x,y are pixel coordinats within the play arey
*/
void st_NewTrashDust(uint8_t x, uint8_t y, int ot)
{
  st_obj *o;
  int8_t objnr = st_NewObj();
  if ( objnr < 0 )
    return;
  o = st_GetObj(objnr);
  o->ot = ot;
  st_SetXY(o, x, y);
  //o->x = (x)<<ST_FP;
  //o->y = (y)<<ST_FP;
  o->x0 = 0;
  o->x1 = 0;
  o->y0 = -2;
  o->y1 = 2;
}

void st_NewTrashDustAreaArgs(int16_t x, int16_t y, int ot)
{
  st_NewTrashDust(x >> ST_FP, y >> ST_FP, ot);
}


void st_NewWall(void)
{
  st_obj *o;
  int8_t objnr = st_NewObj();
  int8_t h;
  if ( objnr < 0 )
    return;
  o = st_GetObj(objnr);
  o->ot = ST_OT_WALL_SOLID;
  h = st_rnd();
  h &= 63;
  h = (int8_t)(((int16_t)h * (int16_t)(ST_AREA_HEIGHT / 4)) >> 6);
  h += ST_AREA_HEIGHT / 6;

  o->x0 = 0;
  o->x1 = 5;
  o->x = (ST_AREA_WIDTH - 1) << ST_FP;

  if ( (st_rnd() & 1) == 0 )
  {
    o->y = (ST_AREA_HEIGHT - 1) << ST_FP;

    o->y0 = -h;
    o->y1 = 0;
  }
  else
  {
    o->y = (0) << ST_FP;
    o->y0 = 0;
    o->y1 = h;
  }
}

void st_NewPlayerMissle(uint8_t x, uint8_t y)
{
  st_obj *o;
  int8_t objnr = st_NewObj();
  if ( objnr < 0 )
    return;
  o = st_GetObj(objnr);
  o->ot = ST_OT_MISSLE;
  st_SetXY(o, x, y);
  //o->x = x<<ST_FP;
  //o->y = y<<ST_FP;
  o->x0 = -4;
  o->x1 = 1;
  o->y0 = 0;
  o->y1 = 0;
}

void st_SetupPlayer(uint8_t objnr, uint8_t ot)
{
  st_obj *o = st_GetObj(objnr);
  switch (ot)
  {
    case ST_OT_PLAYER:
      o->ot = ot;
      o->y0 = -2;
      o->y1 = 2;
      break;
    case ST_OT_PLAYER2:
      o->ot = ot;
      o->y0 = -2;
      o->y1 = 5;
      break;
    case ST_OT_PLAYER3:
      o->ot = ot;
      o->y0 = -5;
      o->y1 = 5;
      break;
  }
}

void st_NewPlayer(void)
{
  st_obj *o;
  int8_t objnr = st_NewObj();
  if ( objnr < 0 )
    return;
  o = st_GetObj(objnr);
  o->x = 6 << ST_FP;
  o->y = (ST_AREA_HEIGHT / 2) << ST_FP;
  o->x0 = -6;
  o->x1 = 0;
  st_SetupPlayer(objnr, ST_OT_PLAYER);
}

/*================================================================*/
/* trash creation */
/*================================================================*/

void st_InitDeltaWall(void)
{
  uint8_t i;
  uint8_t cnt = 0;
  uint8_t max_x = 0;
  uint8_t max_l;

  uint8_t min_dist_for_new = 40;
  uint8_t my_difficulty = st_difficulty;

  if ( st_difficulty >= 2 )
  {

    max_l = ST_AREA_WIDTH;
    max_l -= min_dist_for_new;

    if ( my_difficulty > 30 )
      my_difficulty = 30;
    min_dist_for_new -= my_difficulty;

    for ( i = 0; i < ST_OBJ_CNT; i++ )
    {
      if ( st_objects[i].ot == ST_OT_WALL_SOLID )
      {
        cnt++;
        if ( max_x < (st_objects[i].x >> ST_FP) )
          max_x = (st_objects[i].x >> ST_FP);
      }
    }
    /* if ( cnt < upper_trash_limit ) */
    if ( max_x < max_l )
    {
      st_NewWall();
    }
  }
}


void st_InitDeltaTrash(void)
{
  uint8_t i;
  uint8_t cnt = 0;
  uint8_t max_x = 0;
  uint8_t max_l;

  uint8_t upper_trash_limit = ST_OBJ_CNT - 7;
  uint8_t min_dist_for_new = 20;
  uint8_t my_difficulty = st_difficulty;

  if ( my_difficulty > 14 )
    my_difficulty = 14;
  min_dist_for_new -= my_difficulty;

  for ( i = 0; i < ST_OBJ_CNT; i++ )
  {
    if ( st_objects[i].ot == ST_OT_TRASH1 || st_objects[i].ot == ST_OT_TRASH2 || st_objects[i].ot == ST_OT_GADGET  || st_objects[i].ot == ST_OT_BIG_TRASH )
    {
      cnt++;
      if ( max_x < (st_objects[i].x >> ST_FP) )
        max_x = (st_objects[i].x >> ST_FP);
    }
  }

  max_l = ST_AREA_WIDTH;
  max_l -= min_dist_for_new;

  if ( cnt < upper_trash_limit )
    if ( max_x < max_l )
    {
      if (  (st_difficulty >= 3)  && ((st_rnd() & 7) == 0) )
        st_NewGadget(ST_AREA_WIDTH - 1, rand() & (ST_AREA_HEIGHT - 1));
      else
        st_InitTrash(ST_AREA_WIDTH - 1, rand() & (ST_AREA_HEIGHT - 1), 0 );
    }
}

void st_InitDelta(void)
{
  st_InitDeltaTrash();
  st_InitDeltaWall();
  /*

    uint8_t cnt;
    cnt = st_CntObj(2);
    if ( cnt == 0 )
    st_InitBrick1();
  */
}

/*================================================================*/
/* API: game draw procedure */
/*================================================================*/

void st_DrawInGame(uint8_t fps)
{
  uint8_t i;
  /* draw all objects */
  for ( i = 0; i < ST_OBJ_CNT; i++ )
    st_DrawObj(i);

  //dog_ClrBox(0, ST_AREA_HEIGHT, st_u8g2->width-1, ST_AREA_HEIGHT+3);

  u8g2_SetDrawColor(st_u8g2, 0);
  u8g2_DrawBox(st_u8g2, 0, u8g_height_minus_one - ST_AREA_HEIGHT - 3, st_u8g2->width, 4);

  u8g2_SetDrawColor(st_u8g2, 1);
  u8g2_DrawHLine(st_u8g2, 0, u8g_height_minus_one - ST_AREA_HEIGHT + 1, ST_AREA_WIDTH);
  u8g2_DrawHLine(st_u8g2, 0, u8g_height_minus_one, ST_AREA_WIDTH);
  u8g2_SetFont(st_u8g2, u8g_font_4x6r);
  u8g2_DrawStr(st_u8g2, 0, u8g_height_minus_one - ST_AREA_HEIGHT, st_itoa(st_difficulty));
  u8g2_DrawHLine(st_u8g2, 10, u8g_height_minus_one - ST_AREA_HEIGHT - 3, (st_to_diff_cnt >> ST_DIFF_FP) + 1);
  u8g2_DrawVLine(st_u8g2, 10, u8g_height_minus_one - ST_AREA_HEIGHT - 4, 3);
  u8g2_DrawVLine(st_u8g2, 10 + ST_DIFF_VIS_LEN, u8g_height_minus_one - ST_AREA_HEIGHT - 4, 3);


  /* player points */
  u8g2_DrawStr(st_u8g2, ST_AREA_WIDTH - 5 * 4 - 2, u8g_height_minus_one - ST_AREA_HEIGHT, st_itoa(st_player_points_delayed));


  /* FPS output */
  if ( fps > 0 )
  {
    //i = dog_DrawStr(ST_AREA_WIDTH-5*4-2-7*4, ST_AREA_HEIGHT, font_4x6, "FPS:");
    i = u8g2_DrawStr(st_u8g2, ST_AREA_WIDTH - 5 * 4 - 2 - 7 * 4, u8g_height_minus_one - ST_AREA_HEIGHT, "FPS:");

    //dog_DrawStr(ST_AREA_WIDTH-5*4-2-7*4+i, ST_AREA_HEIGHT, font_4x6, st_itoa(fps));
    u8g2_DrawStr(st_u8g2, ST_AREA_WIDTH - 5 * 4 - 2 - 7 * 4 + i, u8g_height_minus_one - ST_AREA_HEIGHT, st_itoa(fps));
  }
  /*dog_DrawStr(60+i, ST_AREA_HEIGHT, font_4x6, st_itoa(st_CntObj(0)));*/
}

void st_Draw(uint8_t fps)
{
  switch (st_state)
  {
    case ST_STATE_PREPARE:
    case ST_STATE_IPREPARE:
      //dog_DrawStr(0, (st_u8g2->height-6)/2, font_4x6, "SpaceTrash");
      u8g2_SetFont(st_u8g2, u8g_font_4x6r);
      u8g2_SetDrawColor(st_u8g2, 1);
      //dog_DrawStrP(0, (st_u8g2->height-6)/2, font_4x6, DOG_PSTR("SpaceTrash"));
      u8g2_DrawStr(st_u8g2, 0, u8g_height_minus_one - (st_u8g2->height - 6) / 2, "SpaceTrash");
      //dog_SetHLine(st_u8g2->width-st_to_diff_cnt-10, st_u8g2->width-st_to_diff_cnt, (st_u8g2->height-6)/2-1);
      u8g2_DrawHLine(st_u8g2, st_u8g2->width - st_to_diff_cnt - 10, u8g_height_minus_one - (st_u8g2->height - 6) / 2 + 1, 11);
      break;
    case ST_STATE_GAME:
      st_DrawInGame(fps);
      break;
    case ST_STATE_END:
    case ST_STATE_IEND:
      u8g2_SetFont(st_u8g2, u8g_font_4x6r);
      u8g2_SetDrawColor(st_u8g2, 1);
      //dog_DrawStr(0, (st_u8g2->height-6)/2, font_4x6, "Game Over");
      //dog_DrawStrP(0, (st_u8g2->height-6)/2, font_4x6, DOG_PSTR("Game Over"));
      u8g2_DrawStr(st_u8g2, 0, u8g_height_minus_one - (st_u8g2->height - 6) / 2, "Game Over");
      i = 2;
      st_player_points = 9999;
      //dog_DrawStr(50, (st_u8g2->height-6)/2, font_4x6, st_itoa(st_player_points));
      u8g2_DrawStr(st_u8g2, 50, u8g_height_minus_one - (st_u8g2->height - 6) / 2, st_itoa(st_player_points));
      //dog_DrawStr(75, (st_u8g2->height-6)/2, font_4x6, st_itoa(st_highscore));
      u8g2_DrawStr(st_u8g2, 75, u8g_height_minus_one - (st_u8g2->height - 6) / 2, st_itoa(st_highscore));
      //dog_SetHLine(st_to_diff_cnt, st_to_diff_cnt+10, (st_u8g2->height-6)/2-1);
      u8g2_DrawHLine(st_u8g2, st_to_diff_cnt, u8g_height_minus_one - (st_u8g2->height - 6) / 2 + 1, 11);
      break;
  }
}

void st_SetupInGame(void)
{
  st_player_points = 0;
  st_player_points_delayed = 0;
  st_difficulty = 1;
  st_to_diff_cnt = 0;
  st_ClrObjs();
  st_NewPlayer();
  /* st_InitBrick1(); */
}


/*================================================================*/
/* API: game setup */
/*================================================================*/

void st_Setup(u8g2_t *u8g)
{
  st_u8g2 = u8g;
  u8g2_SetBitmapMode(u8g, 1);
  u8g_height_minus_one = u8g->height;
  u8g_height_minus_one--;
}

/*================================================================*/
/* API: game step execution */
/*================================================================*/

/*
  player_pos: 0..255
*/
void st_StepInGame(uint8_t player_pos, uint8_t is_auto_fire, uint8_t is_fire)
{
  uint8_t i, j;
  uint8_t missle_mask;

  /* rescale player pos */
  //st_player_pos = ((uint16_t)player_pos * (uint16_t)ST_AREA_HEIGHT)/256;
  if ( player_pos < 64 )
    st_player_pos = 0;
  else if ( player_pos >= 192 )
    st_player_pos = ST_AREA_HEIGHT - 2 - 1;
  else
    st_player_pos = ((uint16_t)((player_pos - 64)) * (uint16_t)(ST_AREA_HEIGHT - 2)) / 128;
  st_player_pos += 1;
  /* move all objects */
  for ( i = 0; i < ST_OBJ_CNT; i++ )
    st_Move(i);

  /* check for objects which left the play area */
  for ( i = 0; i < ST_OBJ_CNT; i++ )
    if ( st_objects[i].ot != 0 )
      if ( st_IsOut(i) != 0 )
        st_Disappear(i);

  /* missle and destruction handling */
  for ( i = 0; i < ST_OBJ_CNT; i++ )
  {
    missle_mask = st_GetMissleMask(i);
    if ( missle_mask != 0 )           /* should we apply missle handling? */
      if ( st_CalcXY(st_objects + i) != 0 )         /* yes: calculate pixel reference point (st_px_x, st_px_y) */
        for ( j = 0; j < ST_OBJ_CNT; j++ )    /* has any other object been hit? */
          if ( i != j )             /* except missle itself... */
            if ( st_IsHit(j, st_px_x, st_px_y, missle_mask) != 0 )    /* let the member function decide */
            { /* let the member function destroy the object if required */
              st_Destroy(i);
            }
  }

  /* handle fire counter */
  st_FireStep(is_auto_fire, is_fire);

  /* fire */
  for ( i = 0; i < ST_OBJ_CNT; i++ )
    st_Fire(i);

  /* create new objects */
  st_InitDelta();

  /* increase difficulty */

  st_to_diff_cnt++;
  if ( st_to_diff_cnt == (ST_DIFF_VIS_LEN << ST_DIFF_FP) )
  {
    st_to_diff_cnt = 0;
    st_difficulty++;
    st_player_points += ST_POINTS_PER_LEVEL;
  }

  /* update visible player points */
  if ( st_player_points_delayed < st_player_points )
    st_player_points_delayed++;
}

void st_Step(uint8_t player_pos, uint8_t is_auto_fire, uint8_t is_fire)
{
  switch (st_state)
  {
    case ST_STATE_PREPARE:
      st_to_diff_cnt = st_u8g2->width - 10; /* reuse st_to_diff_cnt */
      st_state = ST_STATE_IPREPARE;
      break;
    case ST_STATE_IPREPARE:
      st_to_diff_cnt--;
      if ( st_to_diff_cnt == 0 )
      {
        st_state = ST_STATE_GAME;
        st_SetupInGame();
      }
      break;
    case ST_STATE_GAME:
      st_StepInGame(player_pos, is_auto_fire, is_fire);
      break;
    case ST_STATE_END:
      st_to_diff_cnt = st_u8g2->width - 10; /* reuse st_to_diff_cnt */
      if ( st_highscore < st_player_points)
        st_highscore = st_player_points;
      st_state = ST_STATE_IEND;
      break;
    case ST_STATE_IEND:
      st_to_diff_cnt--;
      if ( st_to_diff_cnt == 0 )
        st_state = ST_STATE_PREPARE;
      break;
  }
}

void setup(void) {
  analogReadResolution(12);
  Serial.begin(9600);
  pinMode(X_pin, INPUT);
  pinMode(SW_pin, INPUT_PULLUP);
  pinMode(MQsensorPin, INPUT);
  pinMode(MoistureSensorPin, INPUT);
  ultrasonic.begin();
  pinMode(ctsPin, INPUT);

  //pinMode(LedSlaveFree, OUTPUT);
  //pinMode(LedACK, OUTPUT);

  //pinMode(TouchSWpin, INPUT_PULLUP);

  pinMode(ACKpin, INPUT);

  //digitalWrite(LedSlaveFree,LOW);
  //digitalWrite(LedACK,LOW);

  // SPI begin
  pinMode(espSS, OUTPUT);
  digitalWrite(espSS, HIGH);
  u8g2.setFont(u8g2_font_5x7_tf);

  pinMode(Y_pin, INPUT);
  u8g2.begin();
}

uint8_t a;
uint8_t b;
uint8_t y = 128;

int checkButton()
{
  int b;
  if (analogRead(Y_pin) < 300) {
    b = 3;
  }
  if (analogRead(Y_pin) > 850) {
    b = 1;
  }
  else
    b = 4;
  delay(250);
  return b;
}

void loop(void)
{
  u8g2.setFont(u8g2_font_6x10_tr);
  u8g2.setFontDirection(0);
  u8g2.setFontRefHeightAll();

  st_Setup(u8g2.getU8g2());
  for (;;)
  {
    st_Step(y, /* is_auto_fire */ 0, /* is_fire */ fire());
    u8g2.firstPage();
    do
    {
      st_Draw(0);
    } while ( u8g2.nextPage() );

    if (digitalRead(SW_pin) == 0)
    {
      i = 2;
      st_player_points = 9999;
      delay(1000);
    }

    if (analogRead(X_pin) < X_Low) {
      y++;
    }
    if (analogRead(X_pin) > X_High) {
      y--;
    }
    if (st_player_points >= k * i) {
      k += 2;
      if (i != 2)
      {
        u8g2.clearBuffer();
        u8g2.setFont(u8g2_font_helvB10_tr);
        u8g2.setCursor(0, 15);
        u8g2.print("Stage Cleared!");
        u8g2.setCursor(0, 40);
        u8g2.print(i);
        u8g2.sendBuffer();
      }
      while (i == 2)
      {
        
        u8g2.setFont(u8g2_font_t0_11_me);
        u8g2.clearBuffer();
        u8g2.setCursor(30, 10);
        u8g2.print("BPM = ");
        u8g2.setCursor(70, 10);
        u8g2.print(accu / j);
        if (BPMValue >= 91)
          u8g2.drawXBMP(30, 15, 64, 56, rock_emoji_bits);
        if (BPMValue <= 89)
          u8g2.drawXBMP(30, 15, 64, 59, calm_icon_bits);

        Serial.println(BPMValue);
        u8g2.sendBuffer();
        if (analogRead(PulseSensorPurplePin) >= 2080)
          one = millis();
        while (analogRead(PulseSensorPurplePin) >= 2080);
        while (analogRead(PulseSensorPurplePin) < 2080);
        two = millis();
        three = abs(two - one);
        if (three > 240 && three < 2400)
        {
          temp = 60000 / (three);
          accu += temp;
          j++;
          if (j == 25)
          {
            j = 1;
            accu = temp;
          }
          if (j == 24)
          {
            BPMValue = accu / 24;
          }
        }
        if (digitalRead(SW_pin) == 0)
        {
          i = 3;
        }
      }
      while (i == 3)
      {
        {
          //int oldval = analogRead(moistSensor); //connect sensor to Analog 0
          //int val = oldval - 2500;
          //print the value to serial port
          analogReadResolution(10);
          sensorRead(t);
          //  spiBus();

          int b = checkButton();


          if (b == 1) t++;//yes
          if (b == 2) negative = true; //no
          if (b == 3) t--;//back

          if (t < 17) {
            u8g2.clearBuffer();
            if (t == 6 && negative)
            { //u8g.setFont(u8g_font_6x10);
              u8g2.setCursor(0, 52);
              u8g2.print("CHECK JUMPER CONTINUITY");
            }
            else
            {
              if (t != 6 && t != 15) {
                instructions(t);
                u8g2.sendBuffer();
                //delay(250);
              }
            }
          }

          /*
            if (t == 6 && MoistureSensorValue > 30)
            {
              SH1106_clear(oled_buf);
              SH1106_bitmap(0, 0, pot_icon, 128, 64, oled_buf);
              SH1106_display(oled_buf);
            }
            else if (t == 6 && MoistureSensorValue <= 30)
            {
              SH1106_clear(oled_buf);
              SH1106_bitmap(0, 0, watering_pot_icon, 128, 64, oled_buf);
              SH1106_display(oled_buf);
            }
          */
          if (t == 15  && MQsensorValue > 16)
          {
            u8g2.clearBuffer();
            //u8g2.setCursor(40, 40);
            //u8g2.print("gasUI Here");
            u8g2.drawXBMP(30, 0, 75, 64, gasUI);
            //SH1106_bitmap(0, 0, gasUI, 128, 64, oled_buf);
            u8g2.sendBuffer();
          }
          if (t == 15 && MQsensorValue <= 16)
          {
            u8g2.clearBuffer();
            //SH1106_bitmap(0, 0, UI_gas_smelly, 128, 64, oled_buf);
            //u8g2.setCursor(40, 40);
            //drawAngry();
            u8g2.drawXBMP(30, 0, 75, 51, UI_gas_smelly);
            //u8g2.print("gasUISmelly Here");
            u8g2.sendBuffer();
          }
        }

      }
      i++;
      delay(1000);
    }
  }
}
