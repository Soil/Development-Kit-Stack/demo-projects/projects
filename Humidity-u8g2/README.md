# Humidity on OLED screen

This is a demo to display the humidity, temperature, and heat index on the OLED screen

## Libraries Used

* U8G2lib.h - For OLED screen
* DHT.h - For DHT22 sensor

## External Pins Used

* Joystick push button - P2_2
* Joystick X Axis - P1_3
* Joystick Y Axis - P1_1
* Moisture sensor - P3_0