#include <U8g2lib.h>
#include <DHT.h>

/// Constructor to set u8g2 pins
U8G2_SH1106_128X64_NONAME_F_4W_HW_SPI u8g2(U8G2_R0, /* cs=*/ P3_5, /* dc=*/ P3_4, /* reset=*/ U8X8_PIN_NONE);

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

#define DHTPIN P3_0           /**< Digital pin connected to the DHT sensor*/
#define DHTTYPE DHT22         /**< DHT 22  (AM2302), AM2321*/
DHT dht(DHTPIN, DHTTYPE);

const int X_pin = P1_3;     /**< Joystick's X Axis connected to P1_3 */
const int Y_pin = A1;       /**< Joystick's Y Axis connected to A1 */
const int SW_pin = P2_2;    /**< Joystick's Button connected to P2_2 */

int temp = 0, x_posi[10], y_posi[10], offset_y = 0, offset_x = 0;
int x = 40, y = 40;

const int X_High = 3272, X_Low = 819, Y_High = 3272, Y_Low = 500;

void setup()
{
  pinMode(SW_pin, INPUT_PULLUP);  //Needs Pull-Up
  dht.begin();
  u8g2.begin();
  u8g2.enableUTF8Print();
}

void loop()
{
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_5x7_tf);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  if (isnan(h) || isnan(t) || isnan(f)) {
    u8g2.setCursor(10, 30);
    u8g2.print("Failed to read from DHT sensor!");
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  if (h != -188)
  {
    u8g2.setCursor(30, 20);
    u8g2.print("Humidity: ");
    u8g2.print(h);
    u8g2.setCursor(30, 40);
    u8g2.print("Temperature: ");
    u8g2.print(t);
    u8g2.setCursor(30, 60);
    u8g2.print("Heat index: ");
    u8g2.print(hic);
    u8g2.sendBuffer();
  }
  delay(50);
}
