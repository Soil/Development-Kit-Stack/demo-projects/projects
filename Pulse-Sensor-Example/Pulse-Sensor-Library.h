/**
 * @brief      This is a class for pulse sensor
 */
class pulse
{

private:
	unsigned long startPulse;							/**<	Stores the time since uC powered ON. It contains the time stamp of 1st peak */
	unsigned long endPulse;								/**<	Stores the time since uC powered ON. It contains the time stamp of 2nd peak */
	unsigned long  periodPulse;							/**<	Stores the time since uC powered ON. It contains the period of the heartbeat */
	int BPMInstant; 									/**<	Stores \a BPM's instantaneous value */
	int pulsePin;										/**<	Stores the attached pulse sensor pin */
	int threshold;										/**<	Threshold to detect the heartbeat */
	int resetBPMcount;									/**<	The count after which the \c accumulatedBPM value will reset */
	int accumulatedBPM;									/**<	Stores the summation of \a BPM values to take the average */
	int BPMAvgValue;									/**<	Stores the average \a BPM value */

public:

	/**
	 * @brief      Constructor of class pulse
	 *
	 * - Initializes \c BPMAvgValue to 0
	 * - Initializes \c resetBPMcount to 0
	 * - Sets \c threshold to 2060
	 */
	pulse()
	{
		BPMAvgValue = 0;
		resetBPMcount = 0;
		accumulatedBPM = 0;
		threshold = 2060;
	}

	/**
	 * @brief      	Function to get the instantaneous pulse data.
	 * @param		none
	 * @returns    	instantaneous \a BPM Value
	 *
	 * \c periodPulse contains the period of heartbeat.
	 * The function will ignore the high and low frequency components (w.r.t heartbeat frequency).
	 * To keep the \a BPM value stable, the function will return time dependent \a BPM value which resets after 5/10 secs.
	 */
	int getPulseData(void)
	{
		if (analogRead(pulsePin) >= threshold)
		{
			startPulse = millis();
		}
		while (analogRead(pulsePin) >= threshold);
		while (analogRead(pulsePin) < threshold);
		endPulse = millis();
		periodPulse = abs(endPulse - startPulse);
		if (periodPulse > 240 && periodPulse < 2400)
		{
			BPMInstant = 60000 / (periodPulse);
			accumulatedBPM += BPMInstant;
			resetBPMcount++;
			if (resetBPMcount == 25)
			{
				resetBPMcount = 1;
				accumulatedBPM = BPMInstant;
			}
		}
		return (accumulatedBPM / resetBPMcount);
	}

	/**
	 * @brief      	Function to get the average pulse data.
	 * @param		none
	 * @returns    	\c BPMAvgValue
	 *
	 * \c periodPulse contains the period of heartbeat.
	 * The function will ignore the high and low frequency components (w.r.t heartbeat frequency).
	 * The \c BPMAvgValue will get updated after every 10 secs approx.
	 */
	int getAvgPulseData(void)
	{
		if (analogRead(pulsePin) >= threshold)
		{
			startPulse = millis();
		}
		while (analogRead(pulsePin) >= threshold);
		while (analogRead(pulsePin) < threshold);
		endPulse = millis();
		periodPulse = abs(endPulse - startPulse);
		if (periodPulse > 240 && periodPulse < 2400)
		{
			BPMInstant = 60000 / (periodPulse);
			accumulatedBPM += BPMInstant;
			resetBPMcount++;
			if (resetBPMcount == 25)
			{
				resetBPMcount = 1;
				accumulatedBPM = BPMInstant;
			}
			if (resetBPMcount == 24)
			{
				BPMAvgValue = accumulatedBPM / 24;
			}
		}
		return (BPMAvgValue);
	}


	/**
	 * @brief      Assigns the pulse sensor pin to the specified pin
	 * @param      pin
	 * @returns	   none
	 *  
	 * \c pin is initialized to \a A0, i.e pulse sensor connected to pin \a A0
	 */
	void pulseSensorPinAssignment(int pin = A0)
	{
		pulsePin = pin;
	}
};