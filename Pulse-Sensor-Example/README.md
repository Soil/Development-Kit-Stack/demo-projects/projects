# Pulse Sensor Demo

This is a demo to print the BPM on the serial monitor.

## Libraries Used

* Pulse-Sensor-Library.h - For pulse sensor

## External Pins Used

* Pulse sensor - P1_0