# Space Trash

This is a space invader game.
Use the joystick to move the spaceship, and fire.

## Libraries Used

* U8G2lib.h - For OLED screen

## External Pins Used

* Joystick push button - P2_2
* Joystick X Axis - P1_3
* Joystick Y Axis - P1_1