#define S0 P4_2
#define S1 P2_6
#define S2 P2_5
#define S3 P4_3
#define sensorOut P2_4

int rmin = 1024, rmax = 0, gmin = 1024, gmax = 0, bmin = 1024, bmax = 0;
// Stores frequency read by the photodiodes
int redFrequency = 0;
int greenFrequency = 0;
int blueFrequency = 0;

void setup() {
  // Setting the outputs
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);

  // Setting the sensorOut as an input
  pinMode(sensorOut, INPUT);

  // Setting frequency scaling to 20%
  digitalWrite(S0, HIGH);
  digitalWrite(S1, LOW);

  // Begins serial communication
  Serial.begin(9600);
}
void loop() {
  // Setting RED (R) filtered photodiodes to be read
  digitalWrite(S2, LOW);
  digitalWrite(S3, LOW);

  // Reading the output frequency
  redFrequency = pulseIn(sensorOut, LOW);
  //redFrequency = map(redFrequency, 430, 770, 255, 0);
  if (redFrequency <= rmin)
    rmin = redFrequency;
  if (redFrequency >= rmax)
    rmax = redFrequency;
  // Printing the RED (R) value
  Serial.print("Rmin = ");
  Serial.print(rmin);
  Serial.print("\tRmax = ");
  Serial.print(rmax);
  
  delay(100);

  // Setting GREEN (G) filtered photodiodes to be read
  digitalWrite(S2, HIGH);
  digitalWrite(S3, HIGH);

  // Reading the output frequency
  greenFrequency = pulseIn(sensorOut, LOW);
  //  greenFrequency = map(greenFrequency, 300, 800, 255, 0);
  if (greenFrequency <= gmin)
    gmin = greenFrequency;
  if (greenFrequency >= gmax)
    gmax = greenFrequency;// Printing the GREEN (G) value
  Serial.print("\tGmin = ");
  Serial.print(gmin);
  Serial.print("\tGmax = ");
  Serial.print(gmax);
    delay(100);

  // Setting BLUE (B) filtered photodiodes to be read
  digitalWrite(S2, LOW);
  digitalWrite(S3, HIGH);

  // Reading the output frequency
  blueFrequency = pulseIn(sensorOut, LOW);
  //blueFrequency = map(blueFrequency, 500, 800, 255, 0);
  if (blueFrequency <= bmin)
    bmin = blueFrequency;
  if (blueFrequency >= bmax)
    bmax = blueFrequency;
  // Printing the BLUE (B) value
  Serial.print("\tBmin = ");
  Serial.print(bmin);
  Serial.print("\tBmax = ");
  Serial.println(bmax);
  
  delay(100);
}
