# Loop - Square - Man

This is a demo code to explain how setup() and loop() works.
Use joystick to move the triangles and make a square. If done correctly, a stickman will walk on its perimeter, else cross mark will appear.

## Libraries Used

* U8G2lib.h - For OLED screen

## External Pins Used

* Joystick push button - P2_2
* Joystick X Axis - P1_3
* Joystick Y Axis - P1_1