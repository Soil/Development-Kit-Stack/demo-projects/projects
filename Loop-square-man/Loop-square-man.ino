#include "msp430.h"
#include <Arduino.h>
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

/// Constructor to set u8g2 pins
U8G2_SH1106_128X64_NONAME_F_4W_HW_SPI u8g2(U8G2_R0, /* cs=*/ P3_5, /* dc=*/ P3_4, /* reset=*/ U8X8_PIN_NONE);

const int X_pin = P1_3;			/**< Joystick's X Axis connected to P1_3 */
const int Y_pin = A1;			/**< Joystick's Y Axis connected to A1 */
const int SW_pin = P2_2;		/**< Joystick's Button connected to P2_2 */

int temp = 0, x_posi[10] = {10, 10, 10, 10, 10, 10, 10, 10, 10}, y_posi[10] = {10, 30, 20, 40, 50, 60, 70, 80, 90}, offset_y = 0, offset_x = 0;
int x = 40, y = 40, i, j;

const int X_High = 3272, X_Low = 900, Y_High = 3272, Y_Low = 500;
const int fastScroll = 7, fastScrollAmt = 70;

/**
 * @brief      Draws stickman
 *
 * @param[in]  stickOffset_x  The stick offset x
 * @param[in]  stickOffset_y  The stick offset y
 * 
 * @returns null
 */
void stickMan(int stickOffset_x, int stickOffset_y)
{
  u8g2.drawPixel(2 + stickOffset_x, 0 + stickOffset_y);
  u8g2.drawPixel(3 + stickOffset_x, 0 + stickOffset_y);
  u8g2.drawPixel(4 + stickOffset_x, 0 + stickOffset_y);
  u8g2.drawPixel(2 + stickOffset_x, 1 + stickOffset_y);
  u8g2.drawPixel(3 + stickOffset_x, 1 + stickOffset_y);
  u8g2.drawPixel(4 + stickOffset_x, 1 + stickOffset_y);
  u8g2.drawPixel(2 + stickOffset_x, 2 + stickOffset_y);
  u8g2.drawPixel(3 + stickOffset_x, 2 + stickOffset_y);
  u8g2.drawPixel(4 + stickOffset_x, 2 + stickOffset_y);
  u8g2.drawPixel(3 + stickOffset_x, 3 + stickOffset_y);
  u8g2.drawPixel(3 + stickOffset_x, 4 + stickOffset_y);
  u8g2.drawPixel(6 + stickOffset_x, 4 + stickOffset_y);
  u8g2.drawPixel(3 + stickOffset_x, 5 + stickOffset_y);
  u8g2.drawPixel(4 + stickOffset_x, 5 + stickOffset_y);
  u8g2.drawPixel(5 + stickOffset_x, 5 + stickOffset_y);
  u8g2.drawPixel(2 + stickOffset_x, 5 + stickOffset_y);
  u8g2.drawPixel(1 + stickOffset_x, 5 + stickOffset_y);
  u8g2.drawPixel(3 + stickOffset_x, 6 + stickOffset_y);
  u8g2.drawPixel(0 + stickOffset_x, 6 + stickOffset_y);
  u8g2.drawPixel(3 + stickOffset_x, 7 + stickOffset_y);
  u8g2.drawPixel(2 + stickOffset_x, 8 + stickOffset_y);
  u8g2.drawPixel(3 + stickOffset_x, 8 + stickOffset_y);
  u8g2.drawPixel(4 + stickOffset_x, 8 + stickOffset_y);
  u8g2.drawPixel(2 + stickOffset_x, 9 + stickOffset_y);
  u8g2.drawPixel(4 + stickOffset_x, 9 + stickOffset_y);
  u8g2.drawPixel(2 + stickOffset_x, 10 + stickOffset_y);
  u8g2.drawPixel(4 + stickOffset_x, 10 + stickOffset_y);
}

/**
 * @brief      Moves the stickman around the square
 *
 * @param[in]  start_x  The start x
 * @param[in]  start_y  The start y
 * @param[in]  width_x  The width x
 * @param[in]  width_y  The width y
 * 
 * @returns null
 */
void moveMan(int start_x, int start_y, int width_x, int width_y)
{
  i = start_x;
  for (j = start_y; j <= start_y + width_y + 22; j++)
  {
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_5x7_tf);
    u8g2.setCursor(10, 10);
    u8g2.print("void loop()");
    u8g2.setFont(u8g2_font_open_iconic_arrow_2x_t);
    u8g2.drawGlyph(x_posi[0] - offset_x, y_posi[0] - offset_y, 76);
    u8g2.drawGlyph(x_posi[1] - offset_x, y_posi[1] - offset_y, 77);
    u8g2.drawGlyph(x_posi[2] - offset_x, y_posi[2] - offset_y, 78);
    u8g2.drawGlyph(x_posi[3] - offset_x, y_posi[3] - offset_y, 79);
    //u8g2.drawFrame(start_x + 8 - offset_x, start_y + 11 - offset_y, width_x - 8, width_y - 11);
    stickMan(i - 12 , j - 25 );
    u8g2.sendBuffer();
    delay(50);
  }
  for (i = start_x ; i <= start_x + width_x + 17 ; i++)
  {
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_5x7_tf);
    u8g2.setCursor(10, 10);
    u8g2.print("void loop()");
    u8g2.setFont(u8g2_font_open_iconic_arrow_2x_t);
    u8g2.drawGlyph(x_posi[0] - offset_x, y_posi[0] - offset_y, 76);
    u8g2.drawGlyph(x_posi[1] - offset_x, y_posi[1] - offset_y, 77);
    u8g2.drawGlyph(x_posi[2] - offset_x, y_posi[2] - offset_y, 78);
    u8g2.drawGlyph(x_posi[3] - offset_x, y_posi[3] - offset_y, 79);
    //u8g2.drawFrame(start_x + 8 - offset_x, start_y + 11 - offset_y, width_x - 8, width_y - 11);
    stickMan(i - 12, j - 25);
    u8g2.sendBuffer();
    delay(50);
  }
  for (j = start_y + width_y + 22 ; j >= start_y ; j--)
  {
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_5x7_tf);
    u8g2.setCursor(10, 10);
    u8g2.print("void loop()");
    u8g2.setFont(u8g2_font_open_iconic_arrow_2x_t);
    u8g2.drawGlyph(x_posi[0] - offset_x, y_posi[0] - offset_y, 76);
    u8g2.drawGlyph(x_posi[1] - offset_x, y_posi[1] - offset_y, 77);
    u8g2.drawGlyph(x_posi[2] - offset_x, y_posi[2] - offset_y, 78);
    u8g2.drawGlyph(x_posi[3] - offset_x, y_posi[3] - offset_y, 79);
    //u8g2.drawFrame(start_x + 8 - offset_x, start_y + 11 - offset_y, width_x - 8, width_y - 11);
    stickMan(i - 12, j - 25);
    u8g2.sendBuffer();
    delay(50);
  }
  for (i = start_x + width_x + 17 ; i >= start_x ; i--)
  {
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_5x7_tf);
    u8g2.setCursor(10, 10);
    u8g2.print("void loop()");
    u8g2.setFont(u8g2_font_open_iconic_arrow_2x_t);
    u8g2.drawGlyph(x_posi[0] - offset_x, y_posi[0] - offset_y, 76);
    u8g2.drawGlyph(x_posi[1] - offset_x, y_posi[1] - offset_y, 77);
    u8g2.drawGlyph(x_posi[2] - offset_x, y_posi[2] - offset_y, 78);
    u8g2.drawGlyph(x_posi[3] - offset_x, y_posi[3] - offset_y, 79);
    //u8g2.drawFrame(start_x + 8 - offset_x, start_y + 11 - offset_y, width_x - 8, width_y - 11);
    stickMan(i - 12 , j - 25);
    u8g2.sendBuffer();
    delay(50);
  }
}

void setup()
{
  pinMode(SW_pin, INPUT_PULLUP);  //Needs Pull-Up
  u8g2.begin();
  u8g2.enableUTF8Print();
}

int start_x, start_y, width_x , width_y ;

void loop()
{
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_5x7_tf);
  u8g2.setCursor(10, 10);
  u8g2.print("void setup()");
  u8g2.setFont(u8g2_font_open_iconic_arrow_2x_t);
  if (analogRead(X_pin) > X_High)
  {
    x++;
    if (temp == fastScroll)  //For fast scroll
    {
      x += fastScrollAmt;
    }

    if (x >= 70)    //X Axis offset for scrolling
    {
      x ++;
      offset_x++;
    }
  }
  else {
    if (analogRead(X_pin) < X_High && analogRead(X_pin) > X_Low)
    { //Do nothing when Joystick at rest
    }
    else
    {
      if (temp == fastScroll) //For fast scroll
      {
        x -= fastScrollAmt;
      }
      x--;
      if (x <= 0)   //X Axis offset for scrolling
      { x--;
        offset_x--;

      }
    }
  }
  if (analogRead(Y_pin) > Y_High)
  {
    y++;
    if (temp == fastScroll) //For fast scroll
    {
      y += fastScrollAmt;
    }
    if (y >= 40)  //Y Axis offset for scrolling
    {
      y++;
      offset_y++;
    }
  }
  else
  {
    if (analogRead(Y_pin) < Y_High && analogRead(Y_pin) > Y_Low)
    {
      ;
    }
    else
    {
      y--;
      if (temp == fastScroll) //For fast scroll
      {
        y -= fastScrollAmt;
      }
      if (y <= 15 )   //Y Axis offset for scrolling
      {
        offset_y--;
        y--;
      }
    }
  }

  if (digitalRead(SW_pin) == 0)   //Next Step
  {
    temp ++;
    delay(200);
  }

  x_posi[temp] = x;     //Updates text placing/scolling position
  y_posi[temp] = y;
  
  switch (temp)
  {
    case 0:         //Place T_Down
      u8g2.drawGlyph(x - offset_x, y - offset_y, 76 );
      break;

    case 1:         //Place T_Left
      u8g2.drawGlyph(x_posi[0] - offset_x, y_posi[0] - offset_y, 76);
      u8g2.drawGlyph(x - offset_x, y - offset_y, 77);
      break;

    case 2:         //Place T_Right
      u8g2.drawGlyph(x_posi[0] - offset_x, y_posi[0] - offset_y, 76);
      u8g2.drawGlyph(x_posi[1] - offset_x, y_posi[1] - offset_y, 77);
      u8g2.drawGlyph(x - offset_x, y - offset_y, 78);
      break;

    case 3:         //Place T_Up
      u8g2.drawGlyph(x_posi[0] - offset_x, y_posi[0] - offset_y, 76);
      u8g2.drawGlyph(x_posi[1] - offset_x, y_posi[1] - offset_y, 77);
      u8g2.drawGlyph(x_posi[2] - offset_x, y_posi[2] - offset_y, 78);
      u8g2.drawGlyph(x - offset_x, y - offset_y, 79);
      break;

    case 4:         //Confirm?
      u8g2.drawGlyph(x_posi[0] - offset_x, y_posi[0] - offset_y, 76);
      u8g2.drawGlyph(x_posi[1] - offset_x, y_posi[1] - offset_y, 77);
      u8g2.drawGlyph(x_posi[2] - offset_x, y_posi[2] - offset_y, 78);
      u8g2.drawGlyph(x_posi[3] - offset_x, y_posi[3] - offset_y, 79);
      break;

    case 5:
      if (((y_posi[1] - offset_y > y_posi[0] - offset_y) && (y_posi[1] - offset_y < y_posi[3] - offset_y))        &&            ((y_posi[2] - offset_y > y_posi[0] - offset_y) && (y_posi[2] - offset_y < y_posi[3] - offset_y))     &&      ((x_posi[0] - offset_x > x_posi[2] - offset_x) && (x_posi[0] - offset_x < x_posi[1] - offset_x))    &&    ((x_posi[3] - offset_x > x_posi[2] - offset_x) && (x_posi[3] - offset_x < x_posi[1] - offset_x)) )
      { //moveMan();
        while (digitalRead(SW_pin) == 1)
        {

          moveMan(x_posi[0] - offset_x, y_posi[0] - offset_y, x_posi[1] - x_posi[2], y_posi[3] - y_posi[0]);
        }
      }
      else
      {
        u8g2.setFont(u8g2_font_open_iconic_check_4x_t);
        u8g2.drawGlyph(50, 50, 68 );
      }
      break;
    default:        //Re-Initialization
      temp = 0;
      offset_y = 0;
      offset_x = 0;
      x = 40;
      y = 40;
      //{
      //  u8g2.setCursor(40, y-offset_y);
      // u8g2.print(y-offset_y);
      // }
      break;

  }
  u8g2.sendBuffer();
  delay(50);
}
