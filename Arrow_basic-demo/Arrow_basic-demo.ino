/*
  Use the (Arduino compatible) u8g2 function "print"  to draw a text.

  Universal 8bit Graphics Library (https://github.com/olikraus/u8g2/)

  Copyright (c) 2016, olikraus@gmail.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list
    of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright notice, this
    list of conditions and the following disclaimer in the documentation and/or other
    materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "msp430.h"
#include <Arduino.h>
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

/*
  U8glib Example Overview:
    Frame Buffer Examples: clearBuffer/sendBuffer. Fast, but may not work with all Arduino boards because of RAM consumption
    Page Buffer Examples: firstPage/nextPage. Less RAM usage, should work with all Arduino boards.
    U8x8 Text Only Example: No RAM usage, direct communication with display controller. No graphics, 8x8 Text only.

*/

// The complete list is available here: https://github.com/olikraus/u8g2/wiki/u8g2setupcpp
// Please update the pin numbers according to your setup. Use U8X8_PIN_NONE if the reset pin is not connected

U8G2_SH1106_128X64_NONAME_F_4W_HW_SPI u8g2(U8G2_R0, /* cs=*/ P3_5, /* dc=*/ P3_4, /* reset=*/ U8X8_PIN_NONE);

const int X_pin = P1_3;     /**< Joystick's X Axis connected to P1_3 */
const int Y_pin = A1;       /**< Joystick's Y Axis connected to A1 */
const int SW_pin = P2_2;    /**< Joystick's Button connected to P2_2 */

int temp = 0, x_posi[10], y_posi[10], offset_y = 0, offset_x = 0;
int x = 40, y = 40;

const int X_High = 3272, X_Low = 900, Y_High = 3272, Y_Low = 500;
const int fastScroll = 7, fastScrollAmt = 70;

void setup()
{
  pinMode(SW_pin, INPUT_PULLUP);  //Needs Pull-Up
  u8g2.begin();
  u8g2.enableUTF8Print();
}

void loop()
{
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_5x7_tf);
  u8g2.setFontDirection(0);
  if (analogRead(X_pin) > X_High)
  {
    x++;
    if (temp == fastScroll)  //For fast scroll
    {
      x += fastScrollAmt;
    }

    if (x >= 70)    //X Axis offset for scrolling
    {
      x ++;
      offset_x++;
    }
  }
  else {
    if (analogRead(X_pin) < X_High && analogRead(X_pin) > X_Low)
    { //Do nothing when Joystick at rest
    }
    else
    {
      if (temp == fastScroll) //For fast scroll
      {
        x -= fastScrollAmt;
      }
      x--;
      if (x <= 0)   //X Axis offset for scrolling
      {
        x--;
        offset_x--;
      }
    }
  }
  if (analogRead(Y_pin) > Y_High)
  {
    y++;
    if (temp == fastScroll) //For fast scroll
    {
      y += fastScrollAmt;
    }
    if (y >= 40)  //Y Axis offset for scrolling
    {
      y++;
      offset_y++;
    }
  }
  else
  {
    if (analogRead(Y_pin) < Y_High && analogRead(Y_pin) > Y_Low)
    {
      ;
    }
    else
    {
      y--;
      if (temp == fastScroll) //For fast scroll
      {
        y -= fastScrollAmt;
      }
      if (y <= 15 )   //Y Axis offset for scrolling
      {
        offset_y--;
        y--;
      }
    }
  }

  if (digitalRead(SW_pin) == 0)   //Next Step
  {
    temp ++;

    delay(200);
  }
  x_posi[temp] = x;     //Updates text placing/scolling position
  y_posi[temp] = y;
  //temp=7;
  switch (temp)
  {
    case 0:         //Place void setup()
      u8g2.setCursor(x - offset_x, y - offset_y);
      u8g2.print("void setup(){");
      break;

    case 1:         //Place void loop()
      u8g2.setCursor(x_posi[0] - offset_x, y_posi[0] - offset_y);
      u8g2.print("void setup(){");
      u8g2.setCursor(x - offset_x, y - offset_y);
      u8g2.print("void loop(){");
      break;

    case 2:         //Place pinMode()
      u8g2.setCursor(x_posi[0] - offset_x, y_posi[0] - offset_y);
      u8g2.print("void setup(){");
      u8g2.setCursor(x_posi[1] - offset_x, y_posi[1] - offset_y);
      u8g2.print("void loop(){");
      u8g2.setCursor(x - offset_x, y - offset_y);
      u8g2.print("pinMode(LED,OUTPUT);}");
      break;

    case 3:         //Place digitalWrite()
      u8g2.setCursor(x_posi[0] - offset_x, y_posi[0] - offset_y);
      u8g2.print("void setup(){");
      u8g2.setCursor(x_posi[1] - offset_x, y_posi[1] - offset_y);
      u8g2.print("void loop(){");
      u8g2.setCursor(x_posi[2] - offset_x, y_posi[2] - offset_y);
      u8g2.print("pinMode(LED,OUTPUT);}");
      u8g2.setCursor(x - offset_x, y - offset_y);
      u8g2.print("digitalWrite(LED,HIGH);");
      break;
    case 4:         //delay()
      u8g2.setCursor(x_posi[0] - offset_x, y_posi[0] - offset_y);
      u8g2.print("void setup(){");
      u8g2.setCursor(x_posi[1] - offset_x, y_posi[1] - offset_y);
      u8g2.print("void loop(){");
      u8g2.setCursor(x_posi[2] - offset_x, y_posi[2] - offset_y);
      u8g2.print("pinMode(LED,OUTPUT);}");
      u8g2.setCursor(x_posi[3] - offset_x, y_posi[3] - offset_y);
      u8g2.print("digitalWrite(LED,HIGH);");
      u8g2.setCursor(x - offset_x, y - offset_y);
      u8g2.print("delay(500);");
      break;
    case 5:         //digitalWrite()
      u8g2.setCursor(x_posi[0] - offset_x, y_posi[0] - offset_y);
      u8g2.print("void setup(){");
      u8g2.setCursor(x_posi[1] - offset_x, y_posi[1] - offset_y);
      u8g2.print("void loop(){");
      u8g2.setCursor(x_posi[2] - offset_x, y_posi[2] - offset_y);
      u8g2.print("pinMode(LED,OUTPUT);}");
      u8g2.setCursor(x_posi[3] - offset_x, y_posi[3] - offset_y);
      u8g2.print("digitalWrite(LED,HIGH);");
      u8g2.setCursor(x_posi[4] - offset_x, y_posi[4] - offset_y);
      u8g2.print("delay(500);");
      u8g2.setCursor(x - offset_x, y - offset_y);
      u8g2.print("digitalWrite(LED,LOW);");
      break;
    case 6:         //delay();}
      u8g2.setCursor(x_posi[0] - offset_x, y_posi[0] - offset_y);
      u8g2.print("void setup(){");
      u8g2.setCursor(x_posi[1] - offset_x, y_posi[1] - offset_y);
      u8g2.print("void loop(){");
      u8g2.setCursor(x_posi[2] - offset_x, y_posi[2] - offset_y);
      u8g2.print("pinMode(LED,OUTPUT);}");
      u8g2.setCursor(x_posi[3] - offset_x, y_posi[3] - offset_y);
      u8g2.print("digitalWrite(LED,HIGH);");
      u8g2.setCursor(x_posi[4] - offset_x, y_posi[4] - offset_y);
      u8g2.print("delay(500);");
      u8g2.setCursor(x_posi[5] - offset_x, y_posi[5] - offset_y);
      u8g2.print("digitalWrite(LED,LOW);");
      u8g2.setCursor(x - offset_x, y - offset_y);
      u8g2.print("delay(500);}");
      break;
    case 7:         //Confirm?
      u8g2.setCursor(x_posi[0] - offset_x, y_posi[0] - offset_y);
      u8g2.print("void setup(){");
      u8g2.setCursor(x_posi[1] - offset_x, y_posi[1] - offset_y);
      u8g2.print("void loop(){");
      u8g2.setCursor(x_posi[2] - offset_x, y_posi[2] - offset_y);
      u8g2.print("pinMode(LED,OUTPUT);}");
      u8g2.setCursor(x_posi[3] - offset_x, y_posi[3] - offset_y);
      u8g2.print("digitalWrite(LED,HIGH);");
      u8g2.setCursor(x_posi[4] - offset_x, y_posi[4] - offset_y);
      u8g2.print("delay(500);");
      u8g2.setCursor(x_posi[5] - offset_x, y_posi[5] - offset_y);
      u8g2.print("digitalWrite(LED,LOW);");
      u8g2.setCursor(x_posi[6] - offset_x, y_posi[6] - offset_y);
      u8g2.print("delay(500);}");
      break;
    case 8:         //Decision Screen
      {
        if (y_posi[6] > y_posi[5] && y_posi[5] > y_posi[4] && y_posi[4] > y_posi[3] && y_posi[3] > y_posi[1] && y_posi[1] > y_posi[2] && y_posi[2] > y_posi[0])
        {
          u8g2.clearBuffer();
          u8g2.setFont(u8g2_font_5x7_tf);
          u8g2.setCursor(x_posi[0] - offset_x, y_posi[0] - offset_y);
          u8g2.print("void setup(){");
          u8g2.setCursor(x_posi[1] - offset_x, y_posi[1] - offset_y);
          u8g2.print("void loop(){");
          u8g2.setCursor(x_posi[2] - offset_x, y_posi[2] - offset_y);
          u8g2.print("pinMode(LED,OUTPUT);}");
          u8g2.setCursor(x_posi[3] - offset_x, y_posi[3] - offset_y);
          u8g2.print("digitalWrite(LED,HIGH);");
          u8g2.setCursor(x_posi[4] - offset_x, y_posi[4] - offset_y);
          u8g2.print("delay(500);");
          u8g2.setCursor(x_posi[5] - offset_x, y_posi[5] - offset_y);
          u8g2.print("digitalWrite(LED,LOW);");
          u8g2.setCursor(x_posi[6] - offset_x, y_posi[6] - offset_y);
          u8g2.print("delay(500);}");
          
          u8g2.setFont(u8g2_font_open_iconic_arrow_2x_t);
          u8g2.drawGlyph(100, y_posi[2] - offset_y + 5, 65 );
          u8g2.sendBuffer();
          delay(250);

          while (digitalRead(SW_pin) == 1)
          {
            u8g2.setFont(u8g2_font_open_iconic_arrow_2x_t);

            u8g2.drawGlyph(100, y_posi[3] - offset_y + 5, 65 );
            u8g2.sendBuffer();
            delay(150);
            u8g2.drawGlyph(100, y_posi[4] - offset_y + 5, 65 );
            u8g2.sendBuffer();
            delay(150);
            u8g2.drawGlyph(100, y_posi[5] - offset_y + 5, 65 );
            u8g2.sendBuffer();
            delay(150);
            u8g2.drawGlyph(100, y_posi[6] - offset_y + 5, 65 );
            u8g2.sendBuffer();
            delay(150);

            u8g2.clearBuffer();
            u8g2.setFont(u8g2_font_5x7_tf);
            u8g2.setCursor(x_posi[0] - offset_x, y_posi[0] - offset_y);
            u8g2.print("void setup(){");
            u8g2.setCursor(x_posi[1] - offset_x, y_posi[1] - offset_y);
            u8g2.print("void loop(){");
            u8g2.setCursor(x_posi[2] - offset_x, y_posi[2] - offset_y);
            u8g2.print("pinMode(LED,OUTPUT);}");
            u8g2.setCursor(x_posi[3] - offset_x, y_posi[3] - offset_y);
            u8g2.print("digitalWrite(LED,HIGH);");
            u8g2.setCursor(x_posi[4] - offset_x, y_posi[4] - offset_y);
            u8g2.print("delay(500);");
            u8g2.setCursor(x_posi[5] - offset_x, y_posi[5] - offset_y);
            u8g2.print("digitalWrite(LED,LOW);");
            u8g2.setCursor(x_posi[6] - offset_x, y_posi[6] - offset_y);
            u8g2.print("delay(500);}");
            u8g2.sendBuffer();
            delay(150);


            if (analogRead(X_pin) > X_High)
            {
              x++;
              if (temp == fastScroll || 8) //For fast scroll
              {
                x += fastScrollAmt;
              }

              if (x >= 70)    //X Axis offset for scrolling
              {
                x ++;
                offset_x++;
              }
            }
            else {
              if (analogRead(X_pin) < X_High && analogRead(X_pin) > X_Low)
              { //Do nothing when Joystick at rest
              }
              else
              {
                if (temp == fastScroll || 8) //For fast scroll
                {
                  x -= fastScrollAmt;
                }
                x--;
                if (x <= 0)   //X Axis offset for scrolling
                {
                  x--;
                  offset_x--;
                }
              }
            }
            if (analogRead(Y_pin) > Y_High)
            {
              y++;
              if (temp == fastScroll || 8) //For fast scroll
              {
                y += fastScrollAmt;
              }
              if (y >= 40)  //Y Axis offset for scrolling
              {
                y ++;
                offset_y++;
              }
            }
            else
            {
              if (analogRead(Y_pin) < Y_High && analogRead(Y_pin) > Y_Low)
              {
                ;
              }
              else
              {
                y--;
                if (temp == fastScroll || 8) //For fast scroll
                {
                  y -= fastScrollAmt;
                }
                if (y <= 15 )   //Y Axis offset for scrolling
                {
                  offset_y--;
                  y--;
                }
              }
            }


          }
          temp = 10;
        }
        else
        {
          u8g2.setFont(u8g2_font_open_iconic_check_4x_t);
          u8g2.drawGlyph(50, 50, 68 );
        }
      }
      break;
    default:        //Re-Initialization
      temp = 0;
      offset_y = 0;
      offset_x = 0;
      x = 40;
      y = 40;
      //{
      //  u8g2.setCursor(40, y-offset_y);
      // u8g2.print(y-offset_y);
      // }
      break;
  }

  delay(10);
  u8g2.sendBuffer();
}
