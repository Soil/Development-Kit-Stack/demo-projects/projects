# Basic Demo - Arrow

This is a demo to explain execution of the code.
Use the joystick to arrange the text displayed. If arranged correctly, arrow till appear accoding to the order of execution, else cross mark will appear.

## Libraries Used

* U8G2lib.h - For OLED screen

## External Pins Used

* Joystick push button - P2_2
* Joystick X Axis - P1_3
* Joystick Y Axis - P1_1