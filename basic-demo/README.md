# Basic Demo

This is a demo to explain the body of the code.
Use the joystick to arrange the text displayed. If arranged correctly, tick mark will appear, else cross mark will appear.

## Libraries Used

* U8G2lib.h - For OLED screen

## External Pins Used

* Joystick push button - P2_2
* Joystick X Axis - P1_3
* Joystick Y Axis - P1_1